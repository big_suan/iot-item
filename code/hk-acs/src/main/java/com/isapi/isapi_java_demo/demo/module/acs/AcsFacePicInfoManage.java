package com.isapi.isapi_java_demo.demo.module.acs;

import com.isapi.isapi_java_demo.demo.entity.DeviceInfoDTO;
import com.isapi.isapi_java_demo.utils.ConfigFileUtil;
import com.isapi.isapi_java_demo.utils.HTTPClientUtil;
import com.isapi.isapi_java_demo.utils.HttpsClientUtil;

import java.util.Map;
import java.util.HashMap;

/**
 * 门禁图片信息管理操作，包括图片增删改查操作
 *
 * @Author: jiangxin14
 * @Date: 2024-01-15  14:15
 */
public class AcsFacePicInfoManage {

    public static final int _BYHTTP_ = 0;//HTTP协议与设备交互

    /**
     * 获取图片库能力集
     *
     * @param deviceInfoDTO 设备信息
     * @return 返回门禁图片库能力集
     */
    public static String GetFaceLibCap(DeviceInfoDTO deviceInfoDTO) {
        String deviceInfoUrl = "/ISAPI/AccessControl/CardInfo/capabilities?format=json";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            try {
                String response = HTTPClientUtil.doGet(deviceInfoDTO, deviceInfoUrl);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            try {
                String response = HttpsClientUtil.httpsGet(deviceInfoUrl);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    /**
     * 在线采集人员图片信息
     *
     * @param deviceInfoDTO 设备信息
     * @return
     */
    public static String CaptureFacePicInfo(DeviceInfoDTO deviceInfoDTO) {
        String deviceInfoUrl = "/ISAPI/AccessControl/CaptureFaceData";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            //从CaptureFacePicInfo.xml引入入参
            Map<String, Object> parameter = new HashMap<>();
            String input = ConfigFileUtil.getReqBodyFromTemplate("acs/AcsFacePicInfoManage/CaptureFacePicInfo.xml", parameter);
            try {
                String response = HTTPClientUtil.doPost(deviceInfoDTO, deviceInfoUrl, input);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * 下发门禁图片
     *
     * @param deviceInfoDTO 设备信息
     * @return
     */
    public static String AddFacePicInfo(DeviceInfoDTO deviceInfoDTO) {
        String deviceInfoUrl = "/ISAPI/Intelligent/FDLib/FaceDataRecord?format=json";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            //从AddFacePicInfo.json引入入参
            Map<String, Object> parameter = new HashMap<>();
            String input = ConfigFileUtil.getReqBodyFromTemplate("acs/AcsFacePicInfoManage/AddFacePicInfo.json", parameter);
            try {
                String response = HTTPClientUtil.doPost(deviceInfoDTO, deviceInfoUrl, input);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * 查询人脸信息，根据工号查询人脸数据
     *
     * @param deviceInfoDTO 设备信息
     * @return
     */
    public static String GetFacePicInfo(DeviceInfoDTO deviceInfoDTO) {
        String deviceInfoUrl = "/ISAPI/Intelligent/FDLib/FDSearch?format=json";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            //从GetFacePicInfo.json引入入参
            Map<String, Object> parameter = new HashMap<>();
            String input = ConfigFileUtil.getReqBodyFromTemplate("acs/AcsFacePicInfoManage/GetFacePicInfo.json", parameter);
            try {
                String response = HTTPClientUtil.doPost(deviceInfoDTO, deviceInfoUrl, input);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }

    }

    /**
     * 删除人员，删除人脸库中的人脸数据，支持批量删除，URI里面需要指定人脸库FDID和人脸库类型
     *
     * @param deviceInfoDTO 设备信息
     * @return
     */
    public static String DeleteFacePicInfo(DeviceInfoDTO deviceInfoDTO) {
        String deviceInfoUrl = "/ISAPI/Intelligent/FDLib/FDSearch/Delete?format=json&FDID=1&faceLibType=blackFD";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            //从DeleteFacePicInfo.json引入入参
            Map<String, Object> parameter = new HashMap<>();
            String input = ConfigFileUtil.getReqBodyFromTemplate("acs/AcsFacePicInfoManage/DeleteFacePicInfo.json", parameter);
            try {
                String response = HTTPClientUtil.doPut(deviceInfoDTO, deviceInfoUrl, input);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }
}
