package com.isapi.isapi_java_demo.demo.module.acs;

import com.isapi.isapi_java_demo.demo.entity.DeviceInfoDTO;
import com.isapi.isapi_java_demo.utils.ConfigFileUtil;
import com.isapi.isapi_java_demo.utils.HTTPClientUtil;
import com.isapi.isapi_java_demo.utils.HttpsClientUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * 门禁卡号信息管理，包括卡号增删改查操作
 *
 * @Author: jiangxin14
 * @Date: 2024-01-15  14:14
 */
public class AcsCardInfoManage {

    public static final int _BYHTTP_ = 0;//HTTP协议与设备交互

    /**
     * 获取门禁卡号信息能力集
     *
     * @param deviceInfoDTO 设备信息
     * @return 能力集返回
     */
    public static String GetCardInfoCap(DeviceInfoDTO deviceInfoDTO) {
        String deviceInfoUrl = "/ISAPI/AccessControl/AcsWorkStatus?format=json";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            try {
                String response = HTTPClientUtil.doGet(deviceInfoDTO, deviceInfoUrl);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            try {
                String response = HttpsClientUtil.httpsGet(deviceInfoUrl);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    /**
     * 下发卡号参数，在指定工号的人员上关联一张卡号
     *
     * @param deviceInfoDTO 设备信息
     * @return
     */
    public static String AddCardInfo(DeviceInfoDTO deviceInfoDTO) {
        String deviceInfoUrl = "/ISAPI/AccessControl/CardInfo/Record?format=json";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            //从AddCardInfo.json引入入参
            Map<String, Object> parameter = new HashMap<>();
            String input = ConfigFileUtil.getReqBodyFromTemplate("acs/AcsCardInfoManage/AddCardInfo.json", parameter);
            try {
                String response = HTTPClientUtil.doPost(deviceInfoDTO, deviceInfoUrl, input);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }

    }

    /**
     * 获取卡号信息参数，指定工号参数获取绑定卡号信息
     *
     * @param deviceInfoDTO 设备信息
     * @return
     */
    public static String GetOneCardInfo(DeviceInfoDTO deviceInfoDTO) {
        String deviceInfoUrl = "/ISAPI/AccessControl/CardInfo/Search?format=json";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            //从GetOneCardInfo.json引入入参
            Map<String, Object> parameter = new HashMap<>();
            String input = ConfigFileUtil.getReqBodyFromTemplate("acs/AcsCardInfoManage/GetOneCardInfo.json", parameter);
            try {
                String response = HTTPClientUtil.doPost(deviceInfoDTO, deviceInfoUrl, input);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * 获取所有卡号信息
     *
     * @param deviceInfoDTO 设备信息
     * @return
     */
    public static String GetAllCardInfo(DeviceInfoDTO deviceInfoDTO) {
        String deviceInfoUrl = "/ISAPI/AccessControl/CardInfo/Search?format=json";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            //从GetAllCardInfo.json引入入参
            Map<String, Object> parameter = new HashMap<>();
            String input = ConfigFileUtil.getReqBodyFromTemplate("acs/AcsCardInfoManage/GetAllCardInfo.json", parameter);
            try {
                String response = HTTPClientUtil.doPost(deviceInfoDTO, deviceInfoUrl, input);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * 删除卡号信息
     *
     * @param deviceInfoDTO 设备信息
     * @return
     */
    public static String DeleteCardInfo(DeviceInfoDTO deviceInfoDTO) {
        String deviceInfoUrl = "/ISAPI/AccessControl/CardInfo/Delete?format=json";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            //从DeleteCardInfo.json引入入参
            Map<String, Object> parameter = new HashMap<>();
            String input = ConfigFileUtil.getReqBodyFromTemplate("acs/AcsCardInfoManage/DeleteCardInfo.json", parameter);
            try {
                String response = HTTPClientUtil.doPut(deviceInfoDTO, deviceInfoUrl, input);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }
}
