package com.isapi.isapi_java_demo.demo.module.acs;

import com.isapi.isapi_java_demo.demo.entity.DeviceInfoDTO;
import com.isapi.isapi_java_demo.utils.ConfigFileUtil;
import com.isapi.isapi_java_demo.utils.HTTPClientUtil;
import com.isapi.isapi_java_demo.utils.HttpsClientUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: jiangxin14
 * @Date: 2024-01-09  16:19
 */
public class AcsCfgInfo {

    public static final int _BYHTTP_ = 0;//HTTP协议与设备交互

    /**
     * 获取设备信息
     *
     * @param deviceInfoDTO 设备信息
     * @return
     */
    public static String GetDeviceInfo(DeviceInfoDTO deviceInfoDTO) {

        String deviceInfoUrl = "/ISAPI/System/deviceInfoDTO?format=json";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            try {
                String response = HTTPClientUtil.doGet(deviceInfoDTO, deviceInfoUrl);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            try {
                String response = HttpsClientUtil.httpsGet(deviceInfoUrl);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    /**
     * 获取门禁主机配置信息
     *
     * @param deviceInfoDTO 设备信息
     * @return
     */
    public static String GetAcsConfig(DeviceInfoDTO deviceInfoDTO) {

        String deviceInfoUrl = "/ISAPI/AccessControl/AcsCfg?format=json";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            try {
                String response = HTTPClientUtil.doGet(deviceInfoDTO, deviceInfoUrl);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            try {
                String response = HttpsClientUtil.httpsGet(deviceInfoUrl);

                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    /**
     * 配置门禁主机参数
     *
     * @param deviceInfoDTO 设备信息
     * @return
     */
    public static String SetAcsConfig(DeviceInfoDTO deviceInfoDTO) {
        String deviceInfoUrl = "/ISAPI/AccessControl/AcsCfg?format=json";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            //从SetAcsConfig.json引入入参
            Map<String, Object> parameter = new HashMap<>();
            String input = ConfigFileUtil.getReqBodyFromTemplate("acs/AcsCfgInfo/SetAcsConfig.json", parameter);
            try {
                String response = HTTPClientUtil.doPut(deviceInfoDTO, deviceInfoUrl, input);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            try {
                String response = HttpsClientUtil.httpsGet(deviceInfoUrl);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    /**
     * 远程控门
     *
     * @param deviceInfoDTO 设备信息 URL:/ISAPI/AccessControl/RemoteControl/door/<ID>   URL中的<ID>代表门编号，65535代表所有门
     * @return
     */
    public static String RemoteDoorCfg(DeviceInfoDTO deviceInfoDTO) {
        String deviceInfoUrl = "/ISAPI/AccessControl/RemoteControl/door/1";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            //从RemoteDoorCfg.xml引入入参
            Map<String, Object> parameter = new HashMap<>();
            String input = ConfigFileUtil.getReqBodyFromTemplate("acs/AcsCfgInfo/RemoteDoorCfg.xml", parameter);
            try {
                String response = HTTPClientUtil.doPut(deviceInfoDTO, deviceInfoUrl, input);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            try {
                String response = HttpsClientUtil.httpsGet(deviceInfoUrl);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    /**
     * 获取门禁设备状态
     *
     * @param deviceInfoDTO 设备信息
     * @return
     */
    public static String AcsWorkStatus(DeviceInfoDTO deviceInfoDTO) {
        String deviceInfoUrl = "/ISAPI/AccessControl/AcsWorkStatus?format=json";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            try {
                String response = HTTPClientUtil.doGet(deviceInfoDTO, deviceInfoUrl);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            try {
                String response = HttpsClientUtil.httpsGet(deviceInfoUrl);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

}
