package com.isapi.isapi_java_demo.demo.module.acs;

import com.isapi.isapi_java_demo.demo.entity.DeviceInfoDTO;
import com.isapi.isapi_java_demo.utils.ConfigFileUtil;
import com.isapi.isapi_java_demo.utils.HTTPClientUtil;
import com.isapi.isapi_java_demo.utils.HttpsClientUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * 人员指纹参数管理，包含指纹数据增删改查
 *
 * @Author: jiangxin14
 * @Date: 2024-01-15  14:18
 */
public class AcsFingerInfoManage {

    public static final int _BYHTTP_ = 0;//HTTP协议与设备交互

    /**
     * 获取指纹参数能力集 GET /ISAPI/AccessControl/FingerPrintCfg/capabilities?format=json
     *
     * @param deviceInfoDTO 设备信息
     * @return 获取能力集
     */
    public static String GetFingerInfoCap(DeviceInfoDTO deviceInfoDTO) {
        String deviceInfoUrl = "/ISAPI/AccessControl/FingerPrintCfg/capabilities?format=json";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            try {
                String response = HTTPClientUtil.doGet(deviceInfoDTO, deviceInfoUrl);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            try {
                String response = HttpsClientUtil.httpsGet(deviceInfoUrl);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }


    /**
     * 在线采集人员指纹数据
     *
     * @param deviceInfoDTO 设备信息
     * @return
     */
    public static String captureFingerInfo(DeviceInfoDTO deviceInfoDTO) {
        String deviceInfoUrl = "/ISAPI/AccessControl/CaptureFingerPrint";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            //从captureFingerInfo.xml引入入参
            Map<String, Object> parameter = new HashMap<>();
            String input = ConfigFileUtil.getReqBodyFromTemplate("acs/AcsFingerInfoManage/captureFingerInfo.xml", parameter);
            try {
                String response = HTTPClientUtil.doPost(deviceInfoDTO, deviceInfoUrl, input);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * 下发指纹数据，指定工号上下发指纹数据
     *
     * @param deviceInfoDTO 设备信息
     * @return
     */
    public static String AddFingerInfo(DeviceInfoDTO deviceInfoDTO) {
        String deviceInfoUrl = "/ISAPI/AccessControl/FingerPrintDownload?format=json";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            //从AddFingerInfo.json引入入参
            Map<String, Object> parameter = new HashMap<>();
            String input = ConfigFileUtil.getReqBodyFromTemplate("acs/AcsFingerInfoManage/AddFingerInfo.json", parameter);
            try {
                String response = HTTPClientUtil.doPost(deviceInfoDTO, deviceInfoUrl, input);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * 获取指纹数据，根据工号获取已经下发的指纹数据
     *
     * @param deviceInfoDTO 设备信息
     * @return 返回指纹数据报文
     */
    public static String GetFingerInfo(DeviceInfoDTO deviceInfoDTO) {
        String deviceInfoUrl = "/ISAPI/AccessControl/FingerPrintUpload?format=json";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            //从GetFingerInfo.json引入入参
            Map<String, Object> parameter = new HashMap<>();
            String input = ConfigFileUtil.getReqBodyFromTemplate("acs/AcsFingerInfoManage/GetFingerInfo.json", parameter);
            try {
                String response = HTTPClientUtil.doPost(deviceInfoDTO, deviceInfoUrl, input);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }

    }

    /**
     * 删除指纹数据，根据工号删除对应的下发的指纹数据
     *
     * @param deviceInfoDTO 设备信息
     * @return 返回指纹数据报文
     */
    public static String DeleteFingerInfo(DeviceInfoDTO deviceInfoDTO) {
        String deviceInfoUrl = "/ISAPI/AccessControl/FingerPrint/Delete?format=json";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            //从DeleteFingerInfo.json引入入参
            Map<String, Object> parameter = new HashMap<>();
            String input = ConfigFileUtil.getReqBodyFromTemplate("acs/AcsFingerInfoManage/DeleteFingerInfo.json", parameter);
            try {
                String response = HTTPClientUtil.doPut(deviceInfoDTO, deviceInfoUrl, input);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }

    }
}
