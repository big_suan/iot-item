package com.isapi.isapi_java_demo.demo.module.acs;

import com.isapi.isapi_java_demo.demo.entity.DeviceInfoDTO;
import com.isapi.isapi_java_demo.utils.ConfigFileUtil;
import com.isapi.isapi_java_demo.utils.HTTPClientUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * 门禁事件查询，包含详细事件查询，事件条数查询
 *
 * @Author: jiangxin14
 * @Date: 2024-01-15  14:15
 */
public class AcsEventSearch {

    public static final int _BYHTTP_ = 0;//HTTP协议与设备交互

    /**
     * 获取门禁事件总条数
     *
     * @param deviceInfoDTO 设备信息
     * @return
     */
    public static String GetEventNum(DeviceInfoDTO deviceInfoDTO) {
        String deviceInfoUrl = "/ISAPI/AccessControl/AcsEventTotalNum?format=json";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            //从GetEventNum.json引入入参
            Map<String, Object> parameter = new HashMap<>();
            String input = ConfigFileUtil.getReqBodyFromTemplate("acs/AcsEventSearch/GetEventNum.json", parameter);
            try {
                String response = HTTPClientUtil.doPost(deviceInfoDTO, deviceInfoUrl, input);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }

    }

    /**
     * 获取详细门禁事件（查询设备中历史门禁事件）
     *
     * @param deviceInfoDTO 设备信息
     * @return
     */
    public static String GetEventDetailInfo(DeviceInfoDTO deviceInfoDTO) {
        String deviceInfoUrl = "/ISAPI/AccessControl/AcsEvent?format=json";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            //从GetEventDetailInfo.json引入入参
            Map<String, Object> parameter = new HashMap<>();
            String input = ConfigFileUtil.getReqBodyFromTemplate("acs/AcsEventSearch/GetEventDetailInfo.json", parameter);
            try {
                String response = HTTPClientUtil.doPost(deviceInfoDTO, deviceInfoUrl, input);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }

    }
}
