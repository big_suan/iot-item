package com.isapi.isapi_java_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IsapiJavaDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(IsapiJavaDemoApplication.class, args);
    }

}
