package com.isapi.isapi_java_demo.demo.module;

import com.isapi.isapi_java_demo.demo.entity.DeviceInfoDTO;
import com.isapi.isapi_java_demo.demo.module.acs.AcsCardInfoManage;
import com.isapi.isapi_java_demo.demo.module.acs.AcsCfgInfo;
import com.isapi.isapi_java_demo.demo.module.acs.AcsEventSearch;
import com.isapi.isapi_java_demo.demo.module.acs.AcsFacePicInfoManage;
import com.isapi.isapi_java_demo.demo.module.acs.AcsFingerInfoManage;
import com.isapi.isapi_java_demo.demo.module.acs.AcsUserInfoManage;
import com.isapi.isapi_java_demo.demo.module.acs.AcsUserPlanManage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.security.saml2.Saml2RelyingPartyProperties;

/**
 * @author zhengxiaohui
 * @date 2024/1/9 16:53
 * @desc 门禁相关demo功能点路由
 */
@Slf4j
public class AcsModuleDispatch {


    public void dispatch(DeviceInfoDTO deviceInfoDTO, String command) {
        switch (command) {
            case "10001": {
                log.info("\n[Function]设备信息获取");
                AcsCfgInfo.GetDeviceInfo(deviceInfoDTO);
                break;
            }
            case "10002": {
                log.info("\n[Function]获取门禁主机配置信息");
                AcsCfgInfo.GetAcsConfig(deviceInfoDTO);
                break;
            }
            case "10003": {
                log.info("\n[Function]配置门禁主机参数");
                AcsCfgInfo.SetAcsConfig(deviceInfoDTO);
                break;
            }
            case "10004": {
                log.info("\n[Function]远程控门");
                AcsCfgInfo.RemoteDoorCfg(deviceInfoDTO);
                break;
            }
            case "10005": {
                log.info("\n[Function]获取门禁状态");
                AcsCfgInfo.AcsWorkStatus(deviceInfoDTO);
                break;
            }
            case "10006": {
                log.info("\n[Function]获取门禁人员信息能力集");
                AcsUserInfoManage.GetAcsUserInfoCap(deviceInfoDTO);
                break;
            }
            case "10008": {
                log.info("\n[Function]下发人员");
                AcsUserInfoManage.AddUserInfo(deviceInfoDTO);
                break;
            }
            case "10009": {
                log.info("\n[Function]查询人员");
                AcsUserInfoManage.GetUserInfo(deviceInfoDTO);
                break;
            }
            case "10010": {
                log.info("\n[Function]查询所有人员");
                AcsUserInfoManage.GetAllUserInfo(deviceInfoDTO);
                break;
            }
            case "10011": {
                log.info("\n[Function]删除人员");
                AcsUserInfoManage.DeleteUserInfo(deviceInfoDTO);
                break;
            }
            case "10012": {
                log.info("\n[Function]获取删除人员进度");
                AcsUserInfoManage.DeleteProcess(deviceInfoDTO);
                break;
            }
            case "10013": {
                log.info("\n[Function]获取门禁卡号信息能力集");
                AcsCardInfoManage.GetCardInfoCap(deviceInfoDTO);
                break;
            }
            case "10014": {
                log.info("\n[Function]下发卡号");
                AcsCardInfoManage.AddCardInfo(deviceInfoDTO);
                break;
            }
            case "10015": {
                log.info("\n[Function]获取一张卡号");
                AcsCardInfoManage.GetOneCardInfo(deviceInfoDTO);
                break;
            }
            case "10016": {
                log.info("\n[Function]获取所有卡号");
                AcsCardInfoManage.GetAllCardInfo(deviceInfoDTO);
                break;
            }
            case "10017": {
                log.info("\n[Function]删除卡号");
                AcsCardInfoManage.DeleteCardInfo(deviceInfoDTO);
                break;
            }
            case "10018": {
                log.info("\n[Function]获取人员图片能力集");
                AcsFacePicInfoManage.GetFaceLibCap(deviceInfoDTO);
                break;
            }
            case "10019": {
                log.info("\n[Function]下发人员图片");
                AcsFacePicInfoManage.AddFacePicInfo(deviceInfoDTO);
                break;
            }
            case "10020": {
                log.info("\n[Function]查询人员图片");
                AcsFacePicInfoManage.GetFacePicInfo(deviceInfoDTO);
                break;
            }
            case "10021": {
                log.info("\n[Function]删除人员图片");
                AcsFacePicInfoManage.DeleteFacePicInfo(deviceInfoDTO);
                break;
            }
            case "10022": {
                log.info("\n[Function]在线采集人员图片");
                AcsFacePicInfoManage.CaptureFacePicInfo(deviceInfoDTO);
                break;
            }
            case "10023": {
                log.info("\n[Function]获取指纹能力集");
                AcsFingerInfoManage.GetFingerInfoCap(deviceInfoDTO);
                break;
            }
            case "10024": {
                log.info("\n[Function]下发指纹数据");
                AcsFingerInfoManage.AddFingerInfo(deviceInfoDTO);
                break;
            }
            case "10025": {
                log.info("\n[Function]获取指纹数据");
                AcsFingerInfoManage.GetFingerInfo(deviceInfoDTO);
                break;
            }
            case "10026": {
                log.info("\n[Function]删除指纹数据");
                AcsFingerInfoManage.DeleteFingerInfo(deviceInfoDTO);
                break;
            }
            case "10027": {
                log.info("\n[Function]在线采集指纹数据");
                AcsFingerInfoManage.captureFingerInfo(deviceInfoDTO);
                break;
            }
            case "10028": {
                log.info("\n[Function]获取门禁事件总条数");
                AcsEventSearch.GetEventNum(deviceInfoDTO);
                break;
            }
            case "10029": {
                log.info("\n[Function]查询门禁事件");
                AcsEventSearch.GetEventDetailInfo(deviceInfoDTO);
                break;
            }
            case "10030": {
                log.info("\n[Function]获取人员周计划参数");
                AcsUserPlanManage.GetUserRightWeekPlanCfg(deviceInfoDTO, "1");
                break;
            }
            case "10031": {
                log.info("\n[Function]设置人员周计划参数");
                AcsUserPlanManage.SetUserRightWeekPlanCfg(deviceInfoDTO, "1");
                break;
            }
            case "10032": {
                log.info("\n[Function]获取人员假日计划参数");
                AcsUserPlanManage.GetUserRightHolidayPlan(deviceInfoDTO, "1");
                break;
            }
            case "10033": {
                log.info("\n[Function]设置人员假日计划参数");
                AcsUserPlanManage.SetUserRightHolidayPlan(deviceInfoDTO, "1");
                break;
            }
            case "10034": {
                log.info("\n[Function]获取人员假日组计划参数");
                AcsUserPlanManage.GetUserRightHolidayPlanGroup(deviceInfoDTO, "1");
                break;
            }
            case "10035": {
                log.info("\n[Function]设置人员假日组计划参数");
                AcsUserPlanManage.SetUserRightHolidayPlanGroup(deviceInfoDTO, "1");
                break;
            }
            case "10036": {
                log.info("\n[Function]获取人员计划模板");
                AcsUserPlanManage.GetUserRightPlanTemplate(deviceInfoDTO, "1");
                break;
            }
            case "10037": {
                log.info("\n[Function]设置人员计划模板");
                AcsUserPlanManage.SetUserRightPlanTemplate(deviceInfoDTO, "1");
                break;
            }


        }
    }
}
