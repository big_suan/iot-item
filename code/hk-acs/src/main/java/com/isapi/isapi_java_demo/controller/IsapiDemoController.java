package com.isapi.isapi_java_demo.controller;

import com.isapi.isapi_java_demo.demo.entity.DeviceInfoDTO;
import com.isapi.isapi_java_demo.demo.module.AcsModuleDispatch;
import com.isapi.isapi_java_demo.demo.module.BasicModuleDispatch;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhengxiaohui
 * @date 2023/12/22 18:52
 * @desc
 */
@RestController
public class IsapiDemoController {

    @GetMapping(value = "/testDev")
    public String testDev(String command, String devOrder) {
        DeviceInfoDTO deviceInfo = new DeviceInfoDTO();
        if (devOrder == null || devOrder.trim().isEmpty()) {
            deviceInfo.setDevIp("192.168.1.111");
            deviceInfo.setDevPort("80");
            deviceInfo.setUsername("admin");
            deviceInfo.setPassword("123456789abc");
        } else {
            deviceInfo.setDevIp("10.19.37.220");
            deviceInfo.setDevPort("8090");
            deviceInfo.setUsername("admin");
            deviceInfo.setPassword("hik12345");
        }
        return runDemo(command, deviceInfo);
    }

    @GetMapping(value = "/runDemo")
    public String runDemo(String command, DeviceInfoDTO deviceInfo) {
        if (command == null || "".equals(command.trim())) {
            return "请指定command";
        }

        // 这里指令前缀第一位为16进制 0~F
        switch (command.substring(0, 1)) {
            case "f": {
                /**
                 * F0001~F9999 预留【SDK通用服务】示例代码
                 */
                System.out.println("\n[Module]通用的sdk服务实例代码");
                BasicModuleDispatch basicModuleDispatch = new BasicModuleDispatch();
                basicModuleDispatch.dispatch(deviceInfo, command);
                break;
            }
            case "1": {
                /**
                 * 10001~19999 预留【门禁系统】相关的代码实现
                 * 门禁设备相关业务接口
                 */
                System.out.println("\n[Module]门禁系统相关的demo示例代码");
                AcsModuleDispatch acsModuleDispatch = new AcsModuleDispatch();
                acsModuleDispatch.dispatch(deviceInfo, command);
                break;
            }
            case "2": {
                /**
                 * 20001~29999 预留【出入口系统】相关的代码实现
                 * 出入口设备相关业务接口
                 */
                System.out.println("\n[Module]出入口系统相关的demo示例代码");
//                        DeepInMindFunctionDemo.dispatch(str, lLoginID);
                break;
            }
            case "3": {
                /**
                 * 30001~39999 预留【超脑】相关的代码实现
                 * 超脑设备相关业务接口
                 */
                System.out.println("\n[Module]超脑系统相关的demo示例代码");
//                        DeepInMindFunctionDemo.dispatch(str, lLoginID);
                break;
            }
            default: {
                System.out.println("\n未知的指令操作!请重新输入!\n");
            }
        }
        return command;
    }
}
