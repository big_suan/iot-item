package com.isapi.isapi_java_demo.demo.module.acs;

import com.isapi.isapi_java_demo.demo.entity.DeviceInfoDTO;
import com.isapi.isapi_java_demo.utils.ConfigFileUtil;
import com.isapi.isapi_java_demo.utils.HTTPClientUtil;
import com.isapi.isapi_java_demo.utils.HttpsClientUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * 门禁人员计划模板配置，包含周计划，假日计划模板配置
 * <p>
 * Tips：人员卡号(以卡为中心)或者工号(以人为中心)下发的时候需要关联门权限和每个门关联的卡/人员权限计划模板，人员下发参考“AcsUserInfoManage”类中下发人员接口。
 * 卡/人员权限计划是必须配置的，否则下发卡号/人员将没有权限（默认计划模板编号1是全天24小时有权限）。
 * 每一个计划模板可以关联一个周计划和一个假日组计划，假日计划的优先级高于周计划。周计划可以配置周一到周日的时间段，每天支持配置8个不同的时间段；
 * 假日组计划可以关联16个不同的假日计划，每一个假日计划可以配置一个假日的起止日期，日期范围内每天的时间段相同（可以配置不同的8个时间段）。
 * 通过这个计划模板配置，可以实现人员时间权限管理。
 *
 * @Author: jiangxin14
 * @Date: 2024-01-15  14:17
 */
public class AcsUserPlanManage {

    public static final int _BYHTTP_ = 0;//HTTP协议与设备交互

    /**
     * 获取人员周计划模板
     *
     * @param deviceInfoDTO 设备信息
     * @param WeekPlanNo    周计划模板编号
     * @return
     */
    public static String GetUserRightWeekPlanCfg(DeviceInfoDTO deviceInfoDTO, String WeekPlanNo) {
        String deviceInfoUrl = "/ISAPI/AccessControl/UserRightWeekPlanCfg/" + WeekPlanNo + "?format=json";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            try {
                String response = HTTPClientUtil.doGet(deviceInfoDTO, deviceInfoUrl);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            try {
                String response = HttpsClientUtil.httpsGet(deviceInfoUrl);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    /**
     * 设置人员周计划模板
     *
     * @param deviceInfoDTO 设备信息
     * @param WeekPlanNo    周计划模板编号
     * @return
     */
    public static String SetUserRightWeekPlanCfg(DeviceInfoDTO deviceInfoDTO, String WeekPlanNo) {
        String deviceInfoUrl = "/ISAPI/AccessControl/UserRightWeekPlanCfg/" + WeekPlanNo + "?format=json";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            //从SetUserRightWeekPlanCfg.json引入入参
            Map<String, Object> parameter = new HashMap<>();
            String input = ConfigFileUtil.getReqBodyFromTemplate("acs/AcsUserPlanManage/SetUserRightWeekPlanCfg.json", parameter);
            try {
                String response = HTTPClientUtil.doPut(deviceInfoDTO, deviceInfoUrl, input);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * 获取人员假日计划参数
     *
     * @param deviceInfoDTO 设备信息
     * @param HolidayPlanNo 假日计划模板编号
     * @return
     */
    public static String GetUserRightHolidayPlan(DeviceInfoDTO deviceInfoDTO, String HolidayPlanNo) {
        String deviceInfoUrl = "/ISAPI/AccessControl/UserRightHolidayPlanCfg/" + HolidayPlanNo + "?format=json";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            try {
                String response = HTTPClientUtil.doGet(deviceInfoDTO, deviceInfoUrl);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            try {
                String response = HttpsClientUtil.httpsGet(deviceInfoUrl);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    /**
     * 设置人员假日计划参数
     *
     * @param deviceInfoDTO 设备信息
     * @param HolidayPlanNo 假日计划模板编号
     * @return
     */
    public static String SetUserRightHolidayPlan(DeviceInfoDTO deviceInfoDTO, String HolidayPlanNo) {
        String deviceInfoUrl = "/ISAPI/AccessControl/UserRightHolidayPlanCfg/" + HolidayPlanNo + "?format=json";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            //从SetUserRightHolidayPlan.json引入入参
            Map<String, Object> parameter = new HashMap<>();
            String input = ConfigFileUtil.getReqBodyFromTemplate("acs/AcsUserPlanManage/SetUserRightHolidayPlan.json", parameter);
            try {
                String response = HTTPClientUtil.doPut(deviceInfoDTO, deviceInfoUrl, input);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * 获取人员假日组计划参数
     *
     * @param deviceInfoDTO  设备信息
     * @param HolidayGroupNo 假日组计划模板编号
     */
    public static String GetUserRightHolidayPlanGroup(DeviceInfoDTO deviceInfoDTO, String HolidayGroupNo) {
        String deviceInfoUrl = "/ISAPI/AccessControl/UserRightHolidayGroupCfg/" + HolidayGroupNo + "?format=json";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            try {
                String response = HTTPClientUtil.doGet(deviceInfoDTO, deviceInfoUrl);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            try {
                String response = HttpsClientUtil.httpsGet(deviceInfoUrl);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    /**
     * 设置人员假日组计划参数
     *
     * @param deviceInfoDTO  设备信息
     * @param HolidayGroupNo 假日组计划模板编号
     * @return
     */
    public static String SetUserRightHolidayPlanGroup(DeviceInfoDTO deviceInfoDTO, String HolidayGroupNo) {
        String deviceInfoUrl = "/ISAPI/AccessControl/UserRightHolidayGroupCfg/" + HolidayGroupNo + "?format=json";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            //从SetUserRightHolidayPlanGroup.json引入入参
            Map<String, Object> parameter = new HashMap<>();
            String input = ConfigFileUtil.getReqBodyFromTemplate("acs/AcsUserPlanManage/SetUserRightHolidayPlanGroup.json", parameter);
            try {
                String response = HTTPClientUtil.doPut(deviceInfoDTO, deviceInfoUrl, input);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * 获取人员计划模板
     *
     * @param deviceInfoDTO  设备信息
     * @param planTemplateNo 人员计划模板编号
     * @return
     */
    public static String GetUserRightPlanTemplate(DeviceInfoDTO deviceInfoDTO, String planTemplateNo) {
        String deviceInfoUrl = "/ISAPI/AccessControl/UserRightPlanTemplate/" + planTemplateNo + "?format=json";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            try {
                String response = HTTPClientUtil.doGet(deviceInfoDTO, deviceInfoUrl);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            try {
                String response = HttpsClientUtil.httpsGet(deviceInfoUrl);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    /**
     * 设置人员计划模板
     *
     * @param deviceInfoDTO  设备信息
     * @param planTemplateNo 人员计划模板编号
     * @return
     */
    public static String SetUserRightPlanTemplate(DeviceInfoDTO deviceInfoDTO, String planTemplateNo) {
        String deviceInfoUrl = "/ISAPI/AccessControl/UserRightPlanTemplate/" + planTemplateNo + "?format=json";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            //从SetUserRightPlanTemplate.json引入入参
            Map<String, Object> parameter = new HashMap<>();
            String input = ConfigFileUtil.getReqBodyFromTemplate("acs/AcsUserPlanManage/SetUserRightPlanTemplate.json", parameter);
            try {
                String response = HTTPClientUtil.doPut(deviceInfoDTO, deviceInfoUrl, input);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }
}
