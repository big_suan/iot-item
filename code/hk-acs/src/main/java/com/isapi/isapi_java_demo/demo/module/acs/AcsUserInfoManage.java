package com.isapi.isapi_java_demo.demo.module.acs;

import com.isapi.isapi_java_demo.demo.entity.DeviceInfoDTO;
import com.isapi.isapi_java_demo.utils.ConfigFileUtil;
import com.isapi.isapi_java_demo.utils.HTTPClientUtil;
import com.isapi.isapi_java_demo.utils.HttpsClientUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * 门禁人员信息管理，包含人员增删改查操作
 *
 * @Author: jiangxin14
 * @Date: 2024-01-15  14:13
 */
public class AcsUserInfoManage {

    public static final int _BYHTTP_ = 0;

    /**
     * 获取门禁人员信息能力集
     *
     * @param deviceInfoDTO 设备参数
     * @return
     */
    public static String GetAcsUserInfoCap(DeviceInfoDTO deviceInfoDTO) {
        String deviceInfoUrl = "/ISAPI/AccessControl/UserInfo/capabilities?format=json";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            try {
                String response = HTTPClientUtil.doGet(deviceInfoDTO, deviceInfoUrl);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            try {
                String response = HttpsClientUtil.httpsGet(deviceInfoUrl);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    /**
     * 下发门禁人员信息
     *
     * @param deviceInfoDTO 设备参数
     * @return 下发返回信息
     */
    public static String AddUserInfo(DeviceInfoDTO deviceInfoDTO) {
        //重点测试中文姓名情况
        String deviceInfoUrl = "/ISAPI/AccessControl/UserInfo/Record?format=json";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            //从AddUserInfo.json引入入参
            Map<String, Object> parameter = new HashMap<>();
            String input = ConfigFileUtil.getReqBodyFromTemplate("acs/AcsUserInfoManage/AddUserInfo.json", parameter);
            try {
                String response = HTTPClientUtil.doPost(deviceInfoDTO, deviceInfoUrl, input);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * 指定工号获取人员信息
     *
     * @param deviceInfoDTO 设备参数
     * @return
     */
    public static String GetUserInfo(DeviceInfoDTO deviceInfoDTO) {
        String deviceInfoUrl = "/ISAPI/AccessControl/UserInfo/Search?format=json";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            //从GetUserInfo.json引入入参
            Map<String, Object> parameter = new HashMap<>();
            String input = ConfigFileUtil.getReqBodyFromTemplate("acs/AcsUserInfoManage/GetUserInfo.json", parameter);
            try {
                String response = HTTPClientUtil.doPost(deviceInfoDTO, deviceInfoUrl, input);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * 获取所有人员信息
     *
     * @param deviceInfoDTO 设备参数
     * @return
     */
    public static String GetAllUserInfo(DeviceInfoDTO deviceInfoDTO) {
        String deviceInfoUrl = "/ISAPI/AccessControl/UserInfo/Search?format=json";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            //从GetAllUserInfo.json引入入参
            Map<String, Object> parameter = new HashMap<>();
            String input = ConfigFileUtil.getReqBodyFromTemplate("acs/AcsUserInfoManage/GetAllUserInfo.json", parameter);
            try {
                String response = HTTPClientUtil.doPost(deviceInfoDTO, deviceInfoUrl, input);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }

    }

    /**
     * 删除人员信息
     *
     * @param deviceInfoDTO 设备参数
     * @return
     */
    public static String DeleteUserInfo(DeviceInfoDTO deviceInfoDTO) {
        String deviceInfoUrl = "/ISAPI/AccessControl/UserInfo/Delete?format=json";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            //从DeleteUserInfo.json引入入参
            Map<String, Object> parameter = new HashMap<>();
            String input = ConfigFileUtil.getReqBodyFromTemplate("acs/AcsUserInfoManage/DeleteUserInfo.json", parameter);
            try {
                String response = HTTPClientUtil.doPut(deviceInfoDTO, deviceInfoUrl, input);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * 获取删除人员信息进度
     *
     * @param deviceInfoDTO 设备参数
     * @return
     */
    public static String DeleteProcess(DeviceInfoDTO deviceInfoDTO) {
        String deviceInfoUrl = "/ISAPI/AccessControl/UserInfoDetail/DeleteProcess?format=json";
        if (deviceInfoDTO.httpType == _BYHTTP_) {
            try {
                String response = HTTPClientUtil.doGet(deviceInfoDTO, deviceInfoUrl);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            try {
                String response = HttpsClientUtil.httpsGet(deviceInfoUrl);

                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }


}
