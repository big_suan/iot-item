package com.isapi.isapi_java_demo.utils;


import com.isapi.isapi_java_demo.demo.entity.DeviceInfoDTO;
import com.isapi.isapi_java_demo.demo.entity.form.ContentDisposition;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zhengxiaohui
 * @date 2024/1/11 18:52
 * @desc 自定义摘要认证的调用示例代码
 */
public class CustomDigestCallUtilTest {

    public static void main(String[] args) {
        System.out.println(CommonMethod.getResFileAbsPath("audio/测试音频1.mp3"));
    }

    @Test
    public void digestCallSample() throws Exception {
        DeviceInfoDTO deviceInfo = new DeviceInfoDTO();
        deviceInfo.setDevIp("10.19.36.106");
        deviceInfo.setDevPort("80");
        deviceInfo.setUsername("admin");
        deviceInfo.setPassword("hik12345");

        String audioAbsPath = CommonMethod.getResFileAbsPath("audio/测试音频1.mp3");
        File file = new File(audioAbsPath);

        String fileNamePrefix = null;
        String fileNamePostfix = null;
        int lastDotIndex = file.getName().lastIndexOf(".");
        if (lastDotIndex != -1) {
            fileNamePrefix = file.getName().substring(0, lastDotIndex);
        }
        if (lastDotIndex != -1 && lastDotIndex < file.getName().length() - 1) {
            fileNamePostfix = file.getName().substring(lastDotIndex + 1);
        }

        FileInputStream uploadPic = new FileInputStream(file);
        int iFileLen = uploadPic.available();

        List<ContentDisposition> formDataItemList = new ArrayList<>();
        ContentDisposition fieldItem = new ContentDisposition();
        fieldItem.setContentType("application/json");
        fieldItem.setName("CustomAudioInfo");
        fieldItem.setNameValue("{\"CustomAudioInfo\":{\"customAudioID\":0,\"customAudioName\":\"" + fileNamePrefix + "\",\"audioFileFormat\":\"" + fileNamePostfix + "\",\"audioFileSize\":" + iFileLen + "}}");
        formDataItemList.add(fieldItem);
        ContentDisposition fileItem = new ContentDisposition();
        fileItem.setContentType("audio/mpeg");
        fileItem.setName("file");
        fileItem.setFilename(file.getName());
        fileItem.setFileLocalPath(audioAbsPath);
        formDataItemList.add(fileItem);

        String url = "/ISAPI/AccessControl/EventCardLinkageCfg/CustomAudio?format=json";
        CustomDigestCallUtil.digestCall(url, "POST", formDataItemList, deviceInfo);

    }
}