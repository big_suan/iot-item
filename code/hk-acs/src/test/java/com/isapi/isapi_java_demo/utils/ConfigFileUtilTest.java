package com.isapi.isapi_java_demo.utils;


import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zhengxiaohui
 * @date 2024/1/11 14:02
 * @desc
 */

public class ConfigFileUtilTest {

    @Test
    public void getReqBodyFromTemplate() {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("employeeNo", "989898");
        parameter.put("name", "测试账号98");
        parameter.put("enable", true);
        parameter.put("doorNo", 1);
        System.out.println(ConfigFileUtil.getReqBodyFromTemplate("isapi/common/Test.json", parameter));
    }

    @Test
    public void getReqBodyFromTemplateFileNotFound() {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("employeeNo", "989898");
        parameter.put("name", "测试账号98");
        parameter.put("enable", true);
        parameter.put("doorNo", 1);
        System.out.println(ConfigFileUtil.getReqBodyFromTemplate("isapi/11/Test.json", parameter));
    }
}