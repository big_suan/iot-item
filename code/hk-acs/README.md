# ISAPI demo模块框架
## !!!本DEMO工程**仅供开发者参考**，方便快速熟悉调用逻辑，不保证工程的稳定性与内容正确性，请自行测试保证您的程序代码的逻辑正确性!
## demo工程使用说明
基于springboot项目搭建的java语言isapi示例demo， 基于bs架构搭建

启动类： com.isapi.isapi_java_demo.IsapiJavaDemoApplication.main
启动占用端口： 默认为 8808 (可以在 application.properties 中修改 server.port 端口值)
启动之后的所有调用url入口在IsapiDemoController中可以查看。

## 工程目录结构说明
com.isapi.isapi_java_demo.controller.IsapiDemoController 
包含示例代码功能所有的演示功能说明与调用入口;
### output 存放输出文件
包括接收到的设备报警事件报文、图片等
### 控制层 com.isapi.isapi_java_demo.controller
主要的demo入口功能示例代码 com.isapi.isapi_java_demo.controller.IsapiDemoController
## 示例功能说明
### 工具类 com.isapi.isapi_java_demo.utils
#### 大文件的摘要认证下发工具 utils.CustomDigestCallUtil
包含功能点： 
- 读取本地文件并构建http摘要认证表单发起调用
### 基础功能
#### ISAPI布防demo示例 function.basic.AlarmGuardDemo 
包含功能点:
1. f0001: 开启设备布防;
2. f0002: 关闭设备布防;
3. f0003: 开启设备订阅(demo仅演示一个event的订阅逻辑);
4. f0004: 关闭设备订阅;
#### 监听demo示例 function.basic.AlarmListenDemo
> 监听端口默认为 9999 (可以在 application.properties 中修改 custom.isapi.listen.port 端口值)
1. f0031: 开启监听;
2. f0032: 关闭监听;
### com.isapi.isapi_java_demo.demo.module 具体的功能点示例
#### com.isapi.isapi_java_demo.demo.module.acs 门禁设备相关功能
##### acs.AcsCfgInfo
包含功能点：
1. 10001:获取设备信息
2. 10002:获取门禁主机配置信息
3. 10003:配置门禁主机参数
4. 10004:远程控门
5. 10005:获取门禁设备状态
##### acs.AcsUserInfoManage
1. 10006:获取门禁人员信息能力集
2. 10008:下发门禁人员信息
3. 10009:指定工号获取人员信息
4. 10010:获取所有人员信息
5. 10011:删除人员信息
6. 10012:获取删除人员信息进度
##### acs.AcsCardInfoManage
   包含功能点：
1. 10013:获取门禁卡号信息能力集
2. 10014:下发卡号参数
3. 10015:获取卡号信息参数
4. 10016:获取所有卡号信息
5. 10017:删除卡号信息
##### acs.AcsFacePicInfoManage
包含功能点：
1. 10018:获取图片库能力集
2. 10019:下发门禁图片
3. 10020:查询人脸信息，根据工号查询人脸数据
4. 10021:删除人员图片
5. 10022:在线采集人员图片信息
##### acs.AcsFingerInfoManage
1. 10023:获取指纹参数能力集
2. 10024:下发指纹数据，指定工号上下发指纹数据
3. 10025:获取指纹数据(根据工号获取)
4. 10026:删除指纹数据(根据工号删除对应的下发的指纹数据)
5. 10027:在线采集人员指纹数据
##### acs.AcsEventSearch
   包含功能点：
1. 10028:获取门禁事件总条数
2. 10029:获取详细门禁事件（查询设备中历史门禁事件）
##### acs.AcsUserPlanManage
1. 10030:获取人员周计划模板
2. 10031:设置人员周计划模板
3. 10032:获取人员假日计划参数
4. 10033:设置人员假日计划参数
5. 10034:获取人员假日组计划参数
6. 10035:设置人员假日组计划参数
7. 10036:获取人员计划模板
8. 10037:设置人员计划模板