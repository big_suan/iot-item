package cc.iotkit.script;

import cc.iotkit.common.utils.JsonUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.HostAccess;
import org.graalvm.polyglot.Value;

/**
 * @author sjg
 */
@Slf4j
public class JavaScriptEngine implements IScriptEngine {

    private final ThreadLocal<Context> contextLocal = ThreadLocal.withInitial(() -> {
        Context context = Context.newBuilder("js")
                .allowHostAccess(HostAccess.ALL)
                .build();
        return context;
    });

    private Value jsScript;

    @Override
    public void setScript(String script) {
        Context context = contextLocal.get();
        jsScript = context.eval("js", String.format(
                "new (function () {\n%s; " +
                        "   this.invoke=function(f,args){" +
                        "       for(i in args){" +
                        "           args[i]=JSON.parse(args[i]);" +
                        "       }" +
                        "       return JSON.stringify(this[f].apply(this,args));" +
                        "   }; " +
                        "})()", script));
    }

    @Override
    public void putScriptEnv(String key, Object value) {
        Context context = contextLocal.get();
        context.getBindings("js").putMember(key, value);
    }

    @Override
    public void invokeMethod(String methodName, Object... args) {
        invokeMethod(new TypeReference<Void>() {
        }, methodName, args);
    }

    @Override
    public <T> T invokeMethod(TypeReference<T> type, String methodName, Object... args) {
        Context context = contextLocal.get();
        Value member = jsScript.getMember("invoke");
        StringBuilder sbArgs = formatArgs(args);
        try {
            // 通过调用 invoke 方法将目标方法返回结果转成 json
            Value rst = member.execute(methodName, args);
            String json = rst.asString();
            log.info("invoke script={}, args={}, result={}", methodName, sbArgs, json);

            // 没有返回值
            if (json == null || "null".equals(json)) {
                return null;
            }
            return JsonUtils.parseObject(json, type);
        } catch (Exception e) {
            log.error("Error invoking JavaScript method", e);
            return null;
        }
    }

    @Override
    public String invokeMethod(String methodName, String args) {
        Context context = contextLocal.get();
        Value member = jsScript.getMember("invoke");
        try {
            // 通过调用 invoke 方法将目标方法返回结果转成 json
            Value rst = member.execute(methodName, JsonUtils.parseArray(args, Object.class));
            String json = rst.asString();
            log.info("invoke script={}, args={}, result={}", methodName, args, json);
            // 没有返回值
            if (json == null || "null".equals(json)) {
                return null;
            }
            return json;
        } catch (Exception e) {
            log.error("Error invoking JavaScript method", e);
            return null;
        }
    }

    private static StringBuilder formatArgs(Object[] args) {
        StringBuilder sbArgs = new StringBuilder("[");
        // 将入参转成 json
        for (int i = 0; i < args.length; i++) {
            args[i] = JsonUtils.toJsonString(args[i]);
            sbArgs.append(args[i]).append(i != args.length - 1 ? "," : "");
        }
        sbArgs.append("]");
        return sbArgs;
    }
}
