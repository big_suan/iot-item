package cc.iotkit.manager.dto.bo.devicesupplier;

import cc.iotkit.common.api.BaseDto;
import cc.iotkit.model.device.DeviceSupplier;
import cc.iotkit.model.device.Manufacturers;
import io.github.linpeilie.annotations.AutoMapper;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;


@ApiModel(value = "DeviceSupplierBo")
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = DeviceSupplier.class, reverseConvertGenerate = false)
public class DeviceSupplierBo extends BaseDto {

    /**
     *
     */
    private String id;

    /**
     * 名称
     */
    @ApiModelProperty(value = "名称")
    @NotNull(message = "名称不能为空")
    private String name;
    /**
     * 地址
     */
    private String address;
    /**
     * 联系人
     */
    private String linkMan;
    /**
     * 联系电话
     */
    private String linkPhone;

    /**
     * 描述
     */
    private String desc;
    /**
     * 创建时间
     */
    private Long createAt;
    /**
     * 创建人
     */
    private String createUserId;
    /**
     * 修改时间
     */
    private Long modifyAt;
    /**
     * 修改人
     */
    private String modifyUserId;

    /**
     * 所属平台用户ID
     */
    private String uid;


}
