package cc.iotkit.manager.controller.report;

import cc.iotkit.common.api.PageRequest;
import cc.iotkit.common.api.Paging;
import cc.iotkit.common.excel.utils.ExcelUtil;
import cc.iotkit.common.log.annotation.Log;
import cc.iotkit.common.log.enums.BusinessType;
import cc.iotkit.common.thing.ThingModelMessage;
import cc.iotkit.common.utils.JsonUtils;


import cc.iotkit.data.constants.KeyConstants;
import cc.iotkit.manager.dto.bo.device.DeviceLogQueryBo;
import cc.iotkit.manager.dto.vo.record.TemperatureHumidityRecordVo;
import cc.iotkit.manager.service.IDeviceManagerService;
import cc.iotkit.model.device.DeviceInfo;
import cc.iotkit.model.report.TemperatureHumidityRecord;
import cc.iotkit.temporal.IThingModelMessageData;
import cn.dev33.satoken.annotation.SaCheckPermission;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 温湿度记录
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/report/temperatureHumidityReport")
public class TemperatureHumidityRecordController {

    @Autowired
    private IDeviceManagerService deviceServiceImpl;

    @Autowired
    private IThingModelMessageData thingModelMessageData;
    private static Validator validator;

    static {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }


    @ApiOperation(value = "获取温湿度记录列表", notes = "获取温湿度记录列表,根据查询条件分页")
    @SaCheckPermission("iot:temperatureHumidityReport:list")
    @PostMapping("/list")
    public Paging<TemperatureHumidityRecordVo> list(@RequestBody @Validated PageRequest<TemperatureHumidityRecord> query) {

        TemperatureHumidityRecord record = query.getData();

        List<DeviceInfo> deviceInfoVoList = deviceServiceImpl.findByPk(KeyConstants.humitureProductKey);
        ;
        List<String> devIds = deviceInfoVoList.stream().map((deviceInfoVo -> deviceInfoVo.getDeviceId())).collect(Collectors.toList());

        PageRequest<DeviceLogQueryBo> request = new PageRequest<>(query.getPageSize(), query.getPageNum(), query.getSortMap());
        DeviceLogQueryBo deviceLogQueryBo = new DeviceLogQueryBo();
        deviceLogQueryBo.setType("property");
        deviceLogQueryBo.setIdentifier("report");

        deviceLogQueryBo.setTimeStart(record.getDateTimeBegin() == null ? 0 : record.getDateTimeBegin().getTime());
        deviceLogQueryBo.setTimeEnd(record.getDateTimeEnd() == null ? 0 : record.getDateTimeEnd().getTime());
        request.setData(deviceLogQueryBo);
        Paging<ThingModelMessage> thingModelMessagePaging = thingModelMessageData.findByTypeAndDeviceIds(devIds, ThingModelMessage.TYPE_PROPERTY,
                "report", query.getPageNum(), query.getPageSize(),
                deviceLogQueryBo.getTimeStart(),
                deviceLogQueryBo.getTimeEnd());

        Paging<TemperatureHumidityRecordVo> temperatureHumidityRecordVoPaging = new Paging<>();
        ArrayList<TemperatureHumidityRecordVo> temperatureHumidityRecordVos = new ArrayList<>();

        temperatureHumidityRecordVoPaging.setTotal(thingModelMessagePaging.getTotal());
        for (ThingModelMessage thingModelMessage : thingModelMessagePaging.getRows()) {
            DeviceInfo deviceInfo = deviceServiceImpl.getDetail(thingModelMessage.getDeviceId());
            TemperatureHumidityRecordVo recordVo = new TemperatureHumidityRecordVo();
            recordVo.setThingModelMessage(thingModelMessage);
            recordVo.setDevice(deviceInfo);
            temperatureHumidityRecordVos.add(recordVo);
        }
        temperatureHumidityRecordVoPaging.setRows(temperatureHumidityRecordVos);

        return temperatureHumidityRecordVoPaging;
    }

    @Log(title = "温湿度记录", businessType = BusinessType.EXPORT)
    @ApiOperation(value = "导出温湿度记录列表", notes = "导出温湿度记录列表")
    @SaCheckPermission("iot:temperatureHumidityReport:export")
    @PostMapping("/export")
    public void export(@Validated @RequestParam String data, HttpServletResponse response) {

        TemperatureHumidityRecord record = JsonUtils.parse(data, TemperatureHumidityRecord.class);


        List<DeviceInfo> deviceInfoVoList = deviceServiceImpl.findByPk(KeyConstants.humitureProductKey);
        ;
        List<String> devIds = deviceInfoVoList.stream().map((deviceInfoVo -> deviceInfoVo.getDeviceId())).collect(Collectors.toList());


        DeviceLogQueryBo deviceLogQueryBo = new DeviceLogQueryBo();
        deviceLogQueryBo.setType("property");
        deviceLogQueryBo.setIdentifier("report");

        deviceLogQueryBo.setTimeStart(record.getDateTimeBegin() == null ? 0 : record.getDateTimeBegin().getTime());
        deviceLogQueryBo.setTimeEnd(record.getDateTimeEnd() == null ? 0 : record.getDateTimeEnd().getTime());

        List<ThingModelMessage> thingModelMessagePaging = thingModelMessageData.findByTypeAndDeviceIdsAll(devIds, ThingModelMessage.TYPE_PROPERTY,
                "report", deviceLogQueryBo.getTimeStart(), deviceLogQueryBo.getTimeEnd());


        ArrayList<TemperatureHumidityRecordVo> temperatureHumidityRecordVos = new ArrayList<>();

        for (ThingModelMessage thingModelMessage : thingModelMessagePaging) {
            DeviceInfo deviceInfo = deviceServiceImpl.getDetail(thingModelMessage.getDeviceId());
            TemperatureHumidityRecordVo recordVo = new TemperatureHumidityRecordVo();
            recordVo.setThingModelMessage(thingModelMessage);
            recordVo.setDevice(deviceInfo);
            recordVo.setFloor(deviceInfo.getFloor());
            recordVo.setPositionName(deviceInfo.getPosition() == null ? "" : deviceInfo.getPosition().getName());
            recordVo.setDeviceName(deviceInfo.getDeviceName());
            recordVo.setDateTime(thingModelMessage.getTime()==null?null:new Date(thingModelMessage.getTime()));
            if (thingModelMessage.getData() != null) {
                Map thingData = (Map) thingModelMessage.getData();
                recordVo.setTemperature(thingData.get("temperature") == null ? null : (Float.parseFloat(thingData.get("temperature").toString())));
                recordVo.setHumidity(thingData.get("humidity") == null ? null : (Float.parseFloat(thingData.get("humidity").toString())));
            }
            temperatureHumidityRecordVos.add(recordVo);
        }

        ExcelUtil.exportExcel(temperatureHumidityRecordVos, "温湿度记录", TemperatureHumidityRecordVo.class, response);
    }


}
