
package cc.iotkit.manager.controller;

import cc.iotkit.common.api.PageRequest;
import cc.iotkit.common.api.Paging;
import cc.iotkit.common.api.Request;
import cc.iotkit.manager.dto.bo.devicesupplier.DeviceSupplierBo;
import cc.iotkit.manager.dto.vo.devicesupplier.DeviceSupplierVo;
import cc.iotkit.manager.service.IDeviceSupplierService;
import cc.iotkit.model.device.DeviceSupplier;
import cn.dev33.satoken.annotation.SaCheckPermission;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Api(tags = {"设备供货商"})
@Slf4j
@RestController
@RequestMapping("/deviceSupplier")
public class DeviceSupplierController {


    @Autowired
    IDeviceSupplierService deviceSupplierService;


    /**
     * 获取列表
     */
    @ApiOperation(value = "获取设备供货商列表")
    @SaCheckPermission("iot:deviceSupplier:list")
    @PostMapping("/list")
    public Paging<DeviceSupplierVo> getDeviceSupplier(
            @Validated @RequestBody PageRequest<DeviceSupplierBo> pageRequest) {
        return deviceSupplierService.selectPageList(pageRequest);
    }

    /**
     * 添加设备供货商
     */
    @ApiOperation(value = "添加设备供货商")
    @SaCheckPermission("iot:deviceSupplier:add")
    @PostMapping("/add")
    public boolean addGroup(@Validated @RequestBody Request<DeviceSupplierBo> boRequest) {
        return deviceSupplierService.addDeviceSupplier(boRequest.getData().to(DeviceSupplier.class));

    }


    /**
     * 修改设备供货商
     */
    @ApiOperation(value = "修改设备供货商")
    @SaCheckPermission("iot:deviceSupplier:edit")
    @PostMapping("/edit")
    public boolean editDeviceSupplier(@RequestBody @Validated Request<DeviceSupplierBo> bo) {
        return deviceSupplierService.updateDeviceSupplier(bo.getData());

    }

    /**
     * 删除分组
     */
    @ApiOperation(value = "删除设备供货商")
    @SaCheckPermission("iot:deviceSupplier:remove")
    @PostMapping("/delete")
    public boolean deleteDeviceSupplier(@Validated @RequestBody Request<String> request) {
        String id = request.getData();
        return deviceSupplierService.deleteDeviceSupplier(id);
    }

}
