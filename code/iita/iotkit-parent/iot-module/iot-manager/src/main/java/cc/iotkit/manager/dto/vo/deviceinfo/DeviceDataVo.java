package cc.iotkit.manager.dto.vo.deviceinfo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value = "DeviceDataVo")
@Data
@ExcelIgnoreUnannotated
public class DeviceDataVo {
    @ApiModelProperty(value = "设备名称")
    @ExcelProperty(value = "设备名称")
    private String deviceName;

    @ApiModelProperty(value = "楼层")
    private String floor;

    @ApiModelProperty(value = "所属分组")
    private String room;

    @ApiModelProperty(value = "时间")
    private Long time;

    private Object data;

}
