package cc.iotkit.manager.dto.vo.devicesupplier;

import cc.iotkit.model.device.DeviceSupplier;
import cc.iotkit.model.device.Manufacturers;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import io.github.linpeilie.annotations.AutoMapper;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


@ApiModel(value = "DeviceSupplierVo")
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = DeviceSupplier.class)
public class DeviceSupplierVo implements Serializable {

    @ApiModelProperty(value = "id")
    @ExcelProperty(value = "id")
    private String id;

    /**
     * 名称
     */
    @ApiModelProperty(value = "名称")
    @ExcelProperty(value = "id")
    private String name;
    /**
     * 地址
     */
    @ExcelProperty(value = "地址")
    private String address;
    /**
     * 联系人
     */
    @ExcelProperty(value = "联系人")
    private String linkMan;
    /**
     * 联系电话
     */
    @ExcelProperty(value = "联系电话")
    private String linkPhone;

    /**
     * 描述
     */
    @ExcelProperty(value = "描述")
    private String desc;
    /**
     * 创建时间
     */
    @ExcelProperty(value = "id")
    private Long createAt;
    /**
     * 创建人
     */
    @ExcelProperty(value = "创建人")
    private String createUserId;
    /**
     * 修改时间
     */
    @ExcelProperty(value = "修改时间")
    private Long modifyAt;
    /**
     * 修改人
     */
    @ExcelProperty(value = "修改人")
    private String modifyUserId;

    /**
     * 所属平台用户ID
     */
    @ExcelProperty(value = "所属平台用户ID")
    private String uid;


}
