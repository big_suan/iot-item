package cc.iotkit.manager.controller.report;

import cc.iotkit.common.api.PageRequest;
import cc.iotkit.common.api.Paging;
import cc.iotkit.common.excel.utils.ExcelUtil;
import cc.iotkit.common.log.annotation.Log;
import cc.iotkit.common.log.enums.BusinessType;
import cc.iotkit.common.utils.JsonUtils;
import cc.iotkit.common.validate.AddGroup;
import cc.iotkit.manager.dto.bo.report.DoorAccessRecordBo;
import cc.iotkit.manager.dto.vo.record.DoorAccessRecordVo;
import cc.iotkit.manager.service.DoorAccessRecordService;
import cc.iotkit.model.report.DoorAccessRecord;
import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaIgnore;
import com.querydsl.core.types.Order;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 门禁记录
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/report/doorAccessReport")
public class DoorAccessRecordController {

    @Autowired
    DoorAccessRecordService doorAccessRecordService;
    private static Validator validator;

    static {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @ApiOperation(value = "增加门禁记录列表", notes = "获取门禁记录列表,根据查询条件分页")
    @SaIgnore()
    @PostMapping("/add")
    public void add(@RequestBody @Validated(AddGroup.class) PageRequest<DoorAccessRecordBo> boPageRequest) {
        DoorAccessRecordBo bo = boPageRequest.getData();
        if (bo != null) {
            Set<ConstraintViolation<DoorAccessRecordBo>> violations = validator.validate(bo);
            if (violations.size() > 0) {
                for (ConstraintViolation<DoorAccessRecordBo> violation : violations) {
                    throw new IllegalArgumentException(violation.getMessage());
                }

            } else {
                doorAccessRecordService.save(bo);
            }
        } else {
            throw new IllegalArgumentException("data为空");
        }


    }


    @ApiOperation(value = "获取门禁记录列表", notes = "获取门禁记录列表,根据查询条件分页")
    @SaCheckPermission("iot:doorAccess:list")
    @PostMapping("/list")
    public Paging<DoorAccessRecordVo> list(@RequestBody @Validated PageRequest<DoorAccessRecord> query) {
        Map map = new HashMap();
        map.put("dateTime", "DESC");
        query.setSortMap(map);
        Paging<DoorAccessRecordVo> paging = doorAccessRecordService.selectPageDoorAccessRecordList(query);
        if (paging!=null && paging.getRows()!=null) {
            List<DoorAccessRecordVo> list = paging.getRows();
            for (DoorAccessRecordVo doorAccessRecordVo : list) {
                switch (doorAccessRecordVo.getUserType()) {
                    case "normal":
                        doorAccessRecordVo.setUserTypeDesc("普通人（主人）");
                        break;
                    case "visitor":
                        doorAccessRecordVo.setUserTypeDesc("来宾（访客）");
                        break;
                    case "blackList":
                        doorAccessRecordVo.setUserTypeDesc("非授权名单人");
                        break;
                    case "administrators":
                        doorAccessRecordVo.setUserTypeDesc("管理员");
                        break;
                    case "operator":
                        doorAccessRecordVo.setUserTypeDesc("操作员");
                        break;
                }
            }
        }
        return paging;
    }

    @Log(title = "门禁记录", businessType = BusinessType.EXPORT)
    @ApiOperation(value = "导出门禁记录列表", notes = "导出门禁记录列表")
    @SaCheckPermission("iot:doorAccess:export")
    @PostMapping("/export")
    public void export(@Validated @RequestParam String data, HttpServletResponse response) {

        DoorAccessRecord request= JsonUtils.parse(data,DoorAccessRecord.class);
        List<DoorAccessRecordVo> list = doorAccessRecordService.selectDoorAccessRecordList(request);
        for (DoorAccessRecordVo doorAccessRecordVo : list) {
            switch (doorAccessRecordVo.getUserType()){
                case "normal":
                    doorAccessRecordVo.setUserTypeDesc("普通人（主人）");
                    break;
                case "visitor":
                    doorAccessRecordVo.setUserTypeDesc("来宾（访客）");
                    break;
                case "blackList":
                    doorAccessRecordVo.setUserTypeDesc("非授权名单人");
                    break;
                case "administrators":
                    doorAccessRecordVo.setUserTypeDesc("管理员");
                    break;
                case "operator":
                    doorAccessRecordVo.setUserTypeDesc("操作员");
                    break;
            }
        }
        ExcelUtil.exportExcel(list, "门禁记录", DoorAccessRecordVo.class, response);
    }


}
