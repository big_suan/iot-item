package cc.iotkit.manager.service;

import cc.iotkit.common.api.PageRequest;
import cc.iotkit.common.api.Paging;
import cc.iotkit.manager.dto.bo.devicesupplier.DeviceSupplierBo;
import cc.iotkit.manager.dto.bo.manufacturers.ManufacturersBo;
import cc.iotkit.manager.dto.vo.devicesupplier.DeviceSupplierVo;
import cc.iotkit.manager.dto.vo.manufacturers.ManufacturersVo;
import cc.iotkit.model.device.DeviceSupplier;
import cc.iotkit.model.device.Manufacturers;

/**
 * @Author: fgq
 * @Date: 2024年3月28日
 * @Version: V1.0
 * @Description: 设备供货商服务
 */
public interface IDeviceSupplierService {

    Paging<DeviceSupplierVo> selectPageList(PageRequest<DeviceSupplierBo> pageRequest);

    boolean deleteDeviceSupplier(String id);

    boolean addDeviceSupplier(DeviceSupplier boRequest);

    boolean updateDeviceSupplier(DeviceSupplierBo data);
}
