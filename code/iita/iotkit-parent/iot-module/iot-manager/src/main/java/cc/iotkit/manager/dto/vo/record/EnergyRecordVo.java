package cc.iotkit.manager.dto.vo.record;

import cc.iotkit.common.api.BaseDto;
import cc.iotkit.common.thing.ThingModelMessage;
import cc.iotkit.model.device.DeviceInfo;
import cc.iotkit.model.report.EnergyRecord;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import io.github.linpeilie.annotations.AutoMapper;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.Date;

@ApiModel(value = "EnergyRecord")
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = EnergyRecord.class, convertGenerate = false)
public class EnergyRecordVo extends BaseDto {

    private static final long serialVersionUID = -1L;


    @ExcelProperty(value = "设备名称", index = 0)
    @ColumnWidth(value = 20)
    private String deviceName;

    @ExcelProperty(value = "楼层", index = 1)
    @ColumnWidth(value = 5)
    private String floor;

    @ExcelProperty(value = "房间", index = 2)
    @ColumnWidth(value = 10)
    private String positionName;

    @ExcelProperty(value = "频率", index = 3)
    private Float hz;

    @ExcelProperty(value = "相电压a", index = 4)
    private Float phaseVoltageA;

    @ExcelProperty(value = "相电压b", index = 5)
    private Float phaseVoltageB;

    @ExcelProperty(value = "相电压C", index = 6)
    private Float phaseVoltageC;


    @ExcelProperty(value = "线电压a", index = 7)
    private Float voltageA;

    @ExcelProperty(value = "线电压b", index = 8)
    private Float voltageB;
    @ExcelProperty(value = "线电压c", index = 9)
    private Float voltageC;


    @ExcelProperty(value = "电流a", index = 10)
    private Float electricityA;

    @ExcelProperty(value = "电流b", index = 11)
    private Float electricityB;

    @ExcelProperty(value = "电流c", index = 12)
    private Float electricityC;


    @ExcelProperty(value = "电能", index = 13)
    private Float electricEnergy;

    @ExcelProperty(value = "功率", index = 14)
    private Float power;


    @ExcelProperty(value = "时间", index = 15)
    private Date dateTime;

    private ThingModelMessage thingModelMessage;

    private DeviceInfo device;

}
