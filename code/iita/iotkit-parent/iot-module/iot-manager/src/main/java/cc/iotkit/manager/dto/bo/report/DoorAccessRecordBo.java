package cc.iotkit.manager.dto.bo.report;

import cc.iotkit.common.api.BaseDto;
import cc.iotkit.model.report.DoorAccessRecord;
import io.github.linpeilie.annotations.AutoMapper;
import io.github.linpeilie.annotations.MapperConfig;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.mapstruct.ReportingPolicy;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;


@ApiModel(value = "DoorAccessRecord")
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = DoorAccessRecord.class, reverseConvertGenerate = false)
@MapperConfig(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public class DoorAccessRecordBo extends BaseDto {

    private static final long serialVersionUID = -1L;

    @NotBlank(message = "设备Id不可为空")
    private String deviceId;

    @NotBlank(message = "设备名称不可为空")
    private String deviceName;

    @NotBlank(message = "员工编号 不可为空")
    private String employeeNo;

    @NotBlank(message = "员工姓名 不可为空")
    private String employeeName;

    @NotNull(message = "开门时间不可为空")
    private Date dateTime;

    @NotBlank(message = "用户类型 不可为空")
    @Pattern(regexp = "^(normal)|(visitor)|(blackList)|(administrators)|(operator)$",message = "用户类型错误，")
    private String userType;
}
