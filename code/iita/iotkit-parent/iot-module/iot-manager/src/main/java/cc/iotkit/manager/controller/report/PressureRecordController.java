package cc.iotkit.manager.controller.report;

import cc.iotkit.common.api.PageRequest;
import cc.iotkit.common.api.Paging;
import cc.iotkit.common.excel.utils.ExcelUtil;
import cc.iotkit.common.log.annotation.Log;
import cc.iotkit.common.log.enums.BusinessType;
import cc.iotkit.common.thing.ThingModelMessage;
import cc.iotkit.common.utils.JsonUtils;
import cc.iotkit.data.constants.KeyConstants;
import cc.iotkit.manager.dto.bo.device.DeviceLogQueryBo;
import cc.iotkit.manager.dto.vo.record.PressureRecordVo;
import cc.iotkit.manager.service.IDeviceManagerService;
import cc.iotkit.model.device.DeviceInfo;
import cc.iotkit.model.report.PressureRecord;
import cc.iotkit.temporal.IThingModelMessageData;
import cn.dev33.satoken.annotation.SaCheckPermission;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 压力记录
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/report/pressureReport")
public class PressureRecordController {

    @Autowired
    private IDeviceManagerService deviceServiceImpl;

    @Autowired
    private IThingModelMessageData thingModelMessageData;
    private static Validator validator;

    static {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }


    @ApiOperation(value = "获取压力记录列表", notes = "获取压力记录列表,根据查询条件分页")
    @SaCheckPermission("iot:pressureReportReport:list")
    @PostMapping("/list")
    public Paging<PressureRecordVo> list(@RequestBody @Validated PageRequest<PressureRecord> query) {

        PressureRecord record = query.getData();

        List<DeviceInfo> deviceInfoVoList = deviceServiceImpl.findByPk(KeyConstants.pressureProductKey);
        ;
        List<String> devIds = deviceInfoVoList.stream().map((deviceInfoVo -> deviceInfoVo.getDeviceId())).collect(Collectors.toList());

        PageRequest<DeviceLogQueryBo> request = new PageRequest<>(query.getPageSize(), query.getPageNum(), query.getSortMap());
        DeviceLogQueryBo deviceLogQueryBo = new DeviceLogQueryBo();
        deviceLogQueryBo.setType("property");
        deviceLogQueryBo.setIdentifier("report");

        deviceLogQueryBo.setTimeStart(record.getDateTimeBegin() == null ? 0 : record.getDateTimeBegin().getTime());
        deviceLogQueryBo.setTimeEnd(record.getDateTimeEnd() == null ? 0 : record.getDateTimeEnd().getTime());
        request.setData(deviceLogQueryBo);
        Paging<ThingModelMessage> thingModelMessagePaging = thingModelMessageData.findByTypeAndDeviceIds(devIds, ThingModelMessage.TYPE_PROPERTY,
                "report", query.getPageNum(), query.getPageSize(),
                deviceLogQueryBo.getTimeStart(),
                deviceLogQueryBo.getTimeEnd());

        Paging<PressureRecordVo> pressureRecordVoPaging = new Paging<>();
        ArrayList<PressureRecordVo> pressureRecordVos = new ArrayList<>();

        pressureRecordVoPaging.setTotal(thingModelMessagePaging.getTotal());
        for (ThingModelMessage thingModelMessage : thingModelMessagePaging.getRows()) {
            DeviceInfo deviceInfo = deviceServiceImpl.getDetail(thingModelMessage.getDeviceId());
            PressureRecordVo recordVo = new PressureRecordVo();
            recordVo.setThingModelMessage(thingModelMessage);
            recordVo.setDevice(deviceInfo);
            pressureRecordVos.add(recordVo);
        }
        pressureRecordVoPaging.setRows(pressureRecordVos);

        return pressureRecordVoPaging;
    }

    @Log(title = "压力记录", businessType = BusinessType.EXPORT)
    @ApiOperation(value = "导出压力记录列表", notes = "导出压力记录列表")
    @SaCheckPermission("iot:pressureReportReport:export")
    @PostMapping("/export")
    public void export(@Validated @RequestParam String data, HttpServletResponse response) {

        PressureRecord record = JsonUtils.parse(data, PressureRecord.class);


        List<DeviceInfo> deviceInfoVoList = deviceServiceImpl.findByPk(KeyConstants.pressureProductKey);
        ;
        List<String> devIds = deviceInfoVoList.stream().map((deviceInfoVo -> deviceInfoVo.getDeviceId())).collect(Collectors.toList());


        DeviceLogQueryBo deviceLogQueryBo = new DeviceLogQueryBo();
        deviceLogQueryBo.setType("property");
        deviceLogQueryBo.setIdentifier("report");

        deviceLogQueryBo.setTimeStart(record.getDateTimeBegin() == null ? 0 : record.getDateTimeBegin().getTime());
        deviceLogQueryBo.setTimeEnd(record.getDateTimeEnd() == null ? 0 : record.getDateTimeEnd().getTime());

        List<ThingModelMessage> thingModelMessagePaging = thingModelMessageData.findByTypeAndDeviceIdsAll(devIds, ThingModelMessage.TYPE_PROPERTY,
                "report", deviceLogQueryBo.getTimeStart(), deviceLogQueryBo.getTimeEnd());


        ArrayList<PressureRecordVo> PressureRecordVos = new ArrayList<>();

        for (ThingModelMessage thingModelMessage : thingModelMessagePaging) {
            DeviceInfo deviceInfo = deviceServiceImpl.getDetail(thingModelMessage.getDeviceId());
            PressureRecordVo recordVo = new PressureRecordVo();
            recordVo.setThingModelMessage(thingModelMessage);
            recordVo.setDevice(deviceInfo);
            recordVo.setFloor(deviceInfo.getFloor());
            recordVo.setPositionName(deviceInfo.getPosition() == null ? "" : deviceInfo.getPosition().getName());
            recordVo.setDeviceName(deviceInfo.getDeviceName());
            recordVo.setDateTime(thingModelMessage.getTime() == null ? null : new Date(thingModelMessage.getTime()));
            if (thingModelMessage.getData() != null) {
                Map thingData = (Map) thingModelMessage.getData();
                recordVo.setPressure(thingData.get("pressure") == null ? null : (Float.parseFloat(thingData.get("pressure").toString())));

            }
            PressureRecordVos.add(recordVo);
        }

        ExcelUtil.exportExcel(PressureRecordVos, "压力记录", PressureRecordVo.class, response);
    }

}
