package cc.iotkit.manager.service;


import cc.iotkit.common.api.PageRequest;
import cc.iotkit.common.api.Paging;
import cc.iotkit.manager.dto.bo.report.DoorAccessRecordBo;
import cc.iotkit.manager.dto.vo.record.DoorAccessRecordVo;
import cc.iotkit.manager.dto.vo.record.DoorAccessStaticVo;
import cc.iotkit.model.report.DoorAccessRecord;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Author: fgq
 * @Date: 2024年3月28日
 * @Version: V1.0
 * @Description: 门禁记录
 */
public interface DoorAccessRecordService {
    Paging<DoorAccessRecordVo> selectPageDoorAccessRecordList(PageRequest<DoorAccessRecord> query);

    List<DoorAccessRecordVo> selectDoorAccessRecordList(DoorAccessRecord recordBo);

    Integer save(DoorAccessRecordBo data);

    /**
     * 按照小时统计数量
     *
     * @param deviceId 设备id
     * @param endTime  统计区间最后日期
     * @return
     */
    List<DoorAccessStaticVo> staticCountByHours(String deviceId, Date endTime);
}
