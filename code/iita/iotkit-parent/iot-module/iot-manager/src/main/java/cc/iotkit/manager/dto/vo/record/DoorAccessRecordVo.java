package cc.iotkit.manager.dto.vo.record;

import cc.iotkit.common.api.BaseDto;
import cc.iotkit.model.report.DoorAccessRecord;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import io.github.linpeilie.annotations.AutoMapper;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.Date;

@ApiModel(value = "DoorAccessRecord")
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = DoorAccessRecord.class,convertGenerate = false)
public class DoorAccessRecordVo extends BaseDto {

    private static final long serialVersionUID = -1L;


    @ExcelProperty(value = "设备Id",index = 0)
    private String deviceId;

    @ExcelProperty(value = "设备名称",index = 1)
    private String deviceName;

    @ExcelProperty(value = "人员编号",index = 2)
    private String employeeNo;

    @ExcelProperty(value = "人员名称",index = 3)
    private String employeeName;

    private String userType;

    @ExcelProperty(value = "人员类型",index = 4)
    private String userTypeDesc;

    @ExcelProperty(value = "开门时间",index = 5)
    private Date dateTime;


    

}
