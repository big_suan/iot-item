package cc.iotkit.manager.service;

import cc.iotkit.common.api.PageRequest;
import cc.iotkit.common.api.Paging;
import cc.iotkit.manager.dto.bo.category.CategoryBo;
import cc.iotkit.manager.dto.bo.manufacturers.ManufacturersBo;
import cc.iotkit.manager.dto.bo.product.ProductBo;
import cc.iotkit.manager.dto.bo.productmodel.ProductModelBo;
import cc.iotkit.manager.dto.bo.thingmodel.ThingModelBo;
import cc.iotkit.manager.dto.vo.category.CategoryVo;
import cc.iotkit.manager.dto.vo.manufacturers.ManufacturersVo;
import cc.iotkit.manager.dto.vo.product.ProductVo;
import cc.iotkit.manager.dto.vo.productmodel.ProductModelVo;
import cc.iotkit.manager.dto.vo.thingmodel.ThingModelVo;
import cc.iotkit.model.device.Manufacturers;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @Author: fgq
 * @Date: 2024年3月28日
 * @Version: V1.0
 * @Description: 生产厂家服务
 */
public interface IManufacturersService {

    Paging<ManufacturersVo> selectPageList(PageRequest<ManufacturersBo> pageRequest);

    boolean deleteManufacturers(String id);

    boolean addManufacturers(Manufacturers boRequest);

    boolean updateManufacturers(ManufacturersBo data);
}
