package cc.iotkit.manager.dto.vo.record;

import cc.iotkit.common.api.BaseDto;
import cc.iotkit.common.thing.ThingModelMessage;
import cc.iotkit.model.device.DeviceInfo;
import cc.iotkit.model.report.TemperatureHumidityRecord;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import io.github.linpeilie.annotations.AutoMapper;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.Date;

@ApiModel(value = "TemperatureHumidityRecord")
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = TemperatureHumidityRecord.class, convertGenerate = false)
public class TemperatureHumidityRecordVo extends BaseDto {

    private static final long serialVersionUID = -1L;


    @ExcelProperty(value = "设备名称", index = 0)
    private String deviceName;

    @ExcelProperty(value = "楼层", index = 1)
    private String floor;

    @ExcelProperty(value = "房间", index = 2)
    private String positionName;

    @ExcelProperty(value = "温度", index = 3)
    private float temperature;

    @ExcelProperty(value = "湿度", index = 4)
    private float humidity;

    @ExcelProperty(value = "时间", index = 5)
    private Date dateTime;

    private ThingModelMessage thingModelMessage;

    private DeviceInfo device;

}
