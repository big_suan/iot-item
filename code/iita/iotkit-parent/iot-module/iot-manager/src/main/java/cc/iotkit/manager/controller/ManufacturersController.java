
package cc.iotkit.manager.controller;

import cc.iotkit.common.api.PageRequest;
import cc.iotkit.common.api.Paging;
import cc.iotkit.common.api.Request;
import cc.iotkit.manager.dto.bo.manufacturers.ManufacturersBo;
import cc.iotkit.manager.dto.vo.manufacturers.ManufacturersVo;
import cc.iotkit.manager.service.IManufacturersService;
import cc.iotkit.model.device.Manufacturers;
import cn.dev33.satoken.annotation.SaCheckPermission;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


@Api(tags = {"生产厂家"})
@Slf4j
@RestController
@RequestMapping("/manufacturers")
public class ManufacturersController {


    @Autowired
    IManufacturersService manufacturersService;



    /**
     * 获取列表
     */
    @ApiOperation(value = "获取生产厂家列表")
    @SaCheckPermission("iot:manufacturers:list")
    @PostMapping("/list")
    public Paging<ManufacturersVo> getManufacturers(
            @Validated @RequestBody PageRequest<ManufacturersBo> pageRequest) {
        return manufacturersService.selectPageList(pageRequest);
    }

    /**
     * 添加生产厂家
     */
    @ApiOperation(value = "添加生产厂家")
    @SaCheckPermission("iot:manufacturers:add")
    @PostMapping("/add")
    public boolean addGroup(@Validated @RequestBody Request<ManufacturersBo> boRequest) {
        return manufacturersService.addManufacturers(boRequest.getData().to(Manufacturers.class));

    }


    /**
     * 修改生产厂家
     */
    @ApiOperation(value = "修改生产厂家")
    @SaCheckPermission("iot:manufacturers:edit")
    @PostMapping("/edit")
    public boolean editManufacturers(@RequestBody @Validated Request<ManufacturersBo> bo) {
        return manufacturersService.updateManufacturers(bo.getData());

    }

    /**
     * 删除分组
     */
    @ApiOperation(value = "删除生产厂家")
    @SaCheckPermission("iot:manufacturers:remove")
    @PostMapping("/delete")
    public boolean deleteManufacturers(@Validated @RequestBody Request<String> request) {
        String id = request.getData();
        return manufacturersService.deleteManufacturers(id);
    }

}
