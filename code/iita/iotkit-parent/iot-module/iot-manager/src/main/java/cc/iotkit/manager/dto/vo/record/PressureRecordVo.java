package cc.iotkit.manager.dto.vo.record;

import cc.iotkit.common.api.BaseDto;
import cc.iotkit.common.thing.ThingModelMessage;
import cc.iotkit.model.device.DeviceInfo;
import cc.iotkit.model.report.TemperatureHumidityRecord;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import io.github.linpeilie.annotations.AutoMapper;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.Date;

@ApiModel(value = "PressureRecord")
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = TemperatureHumidityRecord.class, convertGenerate = false)
public class PressureRecordVo extends BaseDto {

    private static final long serialVersionUID = -1L;


    @ExcelProperty(value = "设备名称", index = 0)
    @ColumnWidth(value = 10)
    private String deviceName;

    @ExcelProperty(value = "楼层", index = 1)
    @ColumnWidth(value = 5)
    private String floor;

    @ExcelProperty(value = "房间", index = 2)
    @ColumnWidth(value = 5)
    private String positionName;

    @ExcelProperty(value = "压力", index = 3)
    @ColumnWidth(value = 5)
    private float pressure;

    @ExcelProperty(value = "时间", index = 4)
    @ColumnWidth(value = 20)
    private Date dateTime;

    private ThingModelMessage thingModelMessage;

    private DeviceInfo device;

}
