/*
 * +----------------------------------------------------------------------
 * | Copyright (c) 奇特物联 2021-2022 All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed 未经许可不能去掉「奇特物联」相关版权
 * +----------------------------------------------------------------------
 * | Author: xw2sy@163.com
 * +----------------------------------------------------------------------
 */
package cc.iotkit.manager.service;

import cc.iotkit.common.api.PageRequest;
import cc.iotkit.common.api.Paging;
import cc.iotkit.common.api.Request;
import cc.iotkit.data.manager.IAlertConfigData;
import cc.iotkit.data.manager.IAlertRecordData;
import cc.iotkit.message.model.Message;
import cc.iotkit.model.alert.AlertConfig;
import cc.iotkit.model.alert.AlertRecord;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.protocol.types.Field;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Slf4j
@Service
public class AlertService {

    @Autowired
    private IAlertConfigData alertConfigData;
    @Autowired
    private IAlertRecordData alertRecordData;

    public AlertConfig createAlertConfig(AlertConfig alertConfig) {
        return alertConfigData.save(alertConfig);
    }

    public AlertConfig updateAlertConfig(AlertConfig alertConfig) {
        return alertConfigData.save(alertConfig);
    }

    public void deleteAlertConfigById(Long id) {
        alertConfigData.deleteById(id);
    }

    public Paging<AlertConfig> selectAlertConfigPage(PageRequest<AlertConfig> request) {
        return alertConfigData.selectAlertConfigPage(request);
    }

    public Paging<AlertRecord> selectAlertRecordPage(PageRequest<AlertRecord> request) {
        return alertRecordData.selectAlertConfigPage(request);
    }

    public void addAlert(AlertConfig config, Message message) {
        String content = message.getFormatContent();
        String deviceId = null;
        String deviceName = null;
        String alarmType = null;
        String alarmTypeName = null;
        if (message!=null && message.getParam()!=null){
            Map<String, Object> map = message.getParam();
            if (map!=null){
                deviceId = map.get("deviceId")+"";
                deviceName = map.get("deviceName")+"";
                alarmType = map.get("alarmType")+"";
                alarmTypeName = map.get("alarmTypeName")+"";
            }
        }
        alertRecordData.save(AlertRecord.builder()
                .alarmType(alarmType)
                .deviceName(deviceName)
                .alarmTypeName(alarmTypeName)
                .deviceId(deviceId)
                .level(config.getLevel())
                .name(config.getName())
                .readFlg(false)
                .alertTime(System.currentTimeMillis())
                .details(content)
                .build());
    }

    public void addAlert(AlertRecord alertRecord) {
        alertRecordData.save(AlertRecord.builder()
                .level(alertRecord.getLevel())
                .name(alertRecord.getName())
                .readFlg(false)
                .deviceName(alertRecord.getDeviceName())
                .deviceId(alertRecord.getDeviceId())
                .alertTime(System.currentTimeMillis())
                .details(alertRecord.getDetails())
                .floor(alertRecord.getFloor())
                .alarmType(alertRecord.getAlarmType())
                .alarmTypeName(alertRecord.getAlarmTypeName())
                .build());
    }

    public void updateAlert(AlertRecord alertRecord){
        alertRecordData.save(alertRecord);
    }

    public AlertRecord getDetail(Long id){
       return alertRecordData.findById(id);
    }
}
