package cc.iotkit.manager.dto.vo.record;

import lombok.Data;

@Data
public class DoorAccessStaticVo {

    private Integer hours;

    private Integer count;

}
