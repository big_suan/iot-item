package cc.iotkit.manager.service.impl;

import cc.iotkit.common.api.PageRequest;
import cc.iotkit.common.api.Paging;
import cc.iotkit.common.enums.ErrCode;
import cc.iotkit.common.exception.BizException;
import cc.iotkit.common.satoken.utils.AuthUtil;
import cc.iotkit.common.utils.MapstructUtils;
import cc.iotkit.common.utils.ReflectUtil;
import cc.iotkit.data.manager.IDeviceSupplierData;
import cc.iotkit.data.manager.IManufacturersData;
import cc.iotkit.manager.dto.bo.devicesupplier.DeviceSupplierBo;
import cc.iotkit.manager.dto.bo.manufacturers.ManufacturersBo;
import cc.iotkit.manager.dto.vo.devicesupplier.DeviceSupplierVo;
import cc.iotkit.manager.dto.vo.manufacturers.ManufacturersVo;
import cc.iotkit.manager.service.DataOwnerService;
import cc.iotkit.manager.service.IDeviceSupplierService;
import cc.iotkit.model.device.DeviceSupplier;
import cc.iotkit.model.device.Manufacturers;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.JdkIdGenerator;

import java.util.Date;

/**
 * @Author: fgq
 * @Date: 2024年3月28日
 * @Version: V1.0
 * @Description: 设备供货商服务实现类
 */

@Slf4j
@RequiredArgsConstructor
@Service
public class DeviceSupplierServiceImpl implements IDeviceSupplierService {

    @Autowired
    private IDeviceSupplierData deviceSupplierData;

    @Autowired
    private DataOwnerService dataOwnerService;


    @Override
    public Paging<DeviceSupplierVo> selectPageList(PageRequest<DeviceSupplierBo> pageRequest) {
        return MapstructUtils.convert(deviceSupplierData.findAll(pageRequest.to(DeviceSupplier.class)), DeviceSupplierVo.class);
    }

    @Override
    public boolean addDeviceSupplier(DeviceSupplier boRequest) {

        boRequest.setUid(AuthUtil.getUserId());

        JdkIdGenerator jdkIdGenerator = new JdkIdGenerator();
        boRequest.setId(jdkIdGenerator.generateId().toString());
        if (deviceSupplierData.findById(boRequest.getId()) != null) {
            throw new BizException(ErrCode.GROUP_ALREADY);
        }
        boRequest.setCreateAt(new Date().getTime());
        boRequest.setCreateUserId(AuthUtil.getUserId());
        boRequest.setUid(AuthUtil.getUserId());
        deviceSupplierData.save(boRequest);
        return true;

    }

    @Override
    public boolean updateDeviceSupplier(DeviceSupplierBo data) {
        DeviceSupplier deviceSupplier = data.to(DeviceSupplier.class);
        if (deviceSupplier.getId() == null) {
            return addDeviceSupplier(deviceSupplier);
        }
        DeviceSupplier oldSupplier = deviceSupplierData.findById(deviceSupplier.getId());
        if (oldSupplier == null) {
            return addDeviceSupplier(deviceSupplier);
        }
        dataOwnerService.checkOwner(oldSupplier);
        ReflectUtil.copyNoNulls(data, oldSupplier);

        oldSupplier.setModifyAt(new Date().getTime());
        oldSupplier.setModifyUserId(AuthUtil.getUserId());
        deviceSupplierData.save(oldSupplier);

        return true;
    }

    @Override
    public boolean deleteDeviceSupplier(String id) {
        deviceSupplierData.deleteById(id);
        return true;
    }
}
