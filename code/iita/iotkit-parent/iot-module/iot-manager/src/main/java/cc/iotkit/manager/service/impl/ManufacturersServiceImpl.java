package cc.iotkit.manager.service.impl;

import cc.iotkit.common.api.PageRequest;
import cc.iotkit.common.api.Paging;
import cc.iotkit.common.enums.ErrCode;
import cc.iotkit.common.exception.BizException;
import cc.iotkit.common.oss.entity.UploadResult;
import cc.iotkit.common.oss.factory.OssFactory;
import cc.iotkit.common.satoken.utils.AuthUtil;
import cc.iotkit.common.utils.JsonUtils;
import cc.iotkit.common.utils.MapstructUtils;
import cc.iotkit.common.utils.ReflectUtil;
import cc.iotkit.data.manager.*;
import cc.iotkit.manager.config.AliyunConfig;
import cc.iotkit.manager.dto.bo.category.CategoryBo;
import cc.iotkit.manager.dto.bo.manufacturers.ManufacturersBo;
import cc.iotkit.manager.dto.bo.product.ProductBo;
import cc.iotkit.manager.dto.bo.productmodel.ProductModelBo;
import cc.iotkit.manager.dto.bo.thingmodel.ThingModelBo;
import cc.iotkit.manager.dto.vo.category.CategoryVo;
import cc.iotkit.manager.dto.vo.manufacturers.ManufacturersVo;
import cc.iotkit.manager.dto.vo.product.ProductVo;
import cc.iotkit.manager.dto.vo.productmodel.ProductModelVo;
import cc.iotkit.manager.dto.vo.thingmodel.ThingModelVo;
import cc.iotkit.manager.service.DataOwnerService;
import cc.iotkit.manager.service.IManufacturersService;
import cc.iotkit.manager.service.IProductService;
import cc.iotkit.model.device.DeviceGroup;
import cc.iotkit.model.device.DeviceInfo;
import cc.iotkit.model.device.Manufacturers;
import cc.iotkit.model.product.Category;
import cc.iotkit.model.product.Product;
import cc.iotkit.model.product.ProductModel;
import cc.iotkit.model.product.ThingModel;
import cc.iotkit.temporal.IDbStructureData;
import cn.hutool.core.lang.UUID;
import com.github.yitter.idgen.YitIdHelper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.mapping.IdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.support.IdGenerators;
import org.springframework.stereotype.Service;
import org.springframework.util.JdkIdGenerator;
import org.springframework.util.SimpleIdGenerator;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @Author: fgq
 * @Date: 2024年3月28日
 * @Version: V1.0
 * @Description: 生产厂家服务实现类
 */

@Slf4j
@RequiredArgsConstructor
@Service
public class ManufacturersServiceImpl implements IManufacturersService {

    @Autowired
    private IManufacturersData manufacturersData;

    @Autowired
    private DataOwnerService dataOwnerService;


    @Override
    public Paging<ManufacturersVo> selectPageList(PageRequest<ManufacturersBo> pageRequest) {
        return MapstructUtils.convert(manufacturersData.findAll(pageRequest.to(Manufacturers.class)), ManufacturersVo.class);
    }

    @Override
    public boolean addManufacturers(Manufacturers boRequest) {

        boRequest.setUid(AuthUtil.getUserId());

        JdkIdGenerator jdkIdGenerator=new JdkIdGenerator();
        boRequest.setId(jdkIdGenerator.generateId().toString());
        if (manufacturersData.findById(boRequest.getId()) != null) {
            throw new BizException(ErrCode.GROUP_ALREADY);
        }
        boRequest.setCreateAt(new Date().getTime());
        boRequest.setCreateUserId(AuthUtil.getUserId());
        boRequest.setUid(AuthUtil.getUserId());
        manufacturersData.save(boRequest);
        return true;

    }

    @Override
    public boolean updateManufacturers(ManufacturersBo data) {
        Manufacturers manufacturers = data.to(Manufacturers.class);
        if(manufacturers.getId()==null){
            return addManufacturers(manufacturers);
        }
        Manufacturers oldManufacturers = manufacturersData.findById(manufacturers.getId());
        if (oldManufacturers == null) {
            return addManufacturers(manufacturers);
        }
        dataOwnerService.checkOwner(oldManufacturers);
        ReflectUtil.copyNoNulls(data, oldManufacturers);

        oldManufacturers.setModifyAt(new Date().getTime());
        oldManufacturers.setModifyUserId(AuthUtil.getUserId());
        manufacturersData.save(oldManufacturers);
        //更新设备中的组信息
        return true;
    }

    @Override
    public boolean deleteManufacturers(String id) {
        manufacturersData.deleteById(id);
        return true;
    }
}
