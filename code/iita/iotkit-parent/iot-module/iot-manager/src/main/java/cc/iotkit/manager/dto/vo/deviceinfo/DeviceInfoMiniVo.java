package cc.iotkit.manager.dto.vo.deviceinfo;

import cc.iotkit.model.device.DeviceInfo;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import io.github.linpeilie.annotations.AutoMapper;
import io.github.linpeilie.annotations.AutoMapping;
import io.github.linpeilie.annotations.ReverseAutoMapping;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@ApiModel(value = "DeviceInfoMiniVo")
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = DeviceInfoVo.class,convertGenerate = false)
public class DeviceInfoMiniVo {
    @ApiModelProperty(value = "设备id")
    @ExcelProperty(value = "设备id")
    private String deviceId;

    @ApiModelProperty(value = "产品名称")
    @ExcelProperty(value = "产品名称")
    private String productName;

    @ApiModelProperty(value = "设备名称")
    @ExcelProperty(value = "设备名称")
    private String deviceName;

    @ApiModelProperty(value = "产品key")
    @ExcelProperty(value = "产品key")
    private String productKey;

    @ApiModelProperty(value = "楼层")
    private String floor;

    @ApiModelProperty(value = "房间")
    private String room;

    @ApiModelProperty(value = "设备属性")
    private Map<String, ?> property = new HashMap<>();

    @ApiModelProperty(value = "设备状态")
    private Boolean online;
}
