package cc.iotkit.openapi.controller;

import cc.iotkit.common.api.PageRequest;
import cc.iotkit.common.api.Paging;
import cc.iotkit.common.api.Request;
import cc.iotkit.data.service.DeviceInfoDataImpl;
import cc.iotkit.manager.dto.vo.product.ProductVo;
import cc.iotkit.manager.service.AlertService;
import cc.iotkit.manager.service.impl.ProductServiceImpl;
import cc.iotkit.model.alert.AlertRecord;
import cc.iotkit.model.device.DeviceInfo;
import cc.iotkit.model.product.Product;
import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import software.amazon.ion.Decimal;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Api(tags = {"openapi-报警中心"})
@Slf4j
@RestController
@RequestMapping("/openapi/alarm")
public class AlarmOpenApi {
    @Autowired
    private AlertService alertService;
    @Autowired
    private DeviceInfoDataImpl deviceInfoDataImpl;
    @Autowired
    private ProductServiceImpl productService;
    @ApiOperation("1、获取报警数量")
    @GetMapping("/v1/getAlarmCount")
    @SaCheckLogin
    public JSONObject getAlarmCount() {
        PageRequest<AlertRecord> request = new PageRequest<>();
        AlertRecord record = new AlertRecord();
        request.setData(record);
        record.setReadFlg(false);
        Paging<AlertRecord> paging = alertService.selectAlertRecordPage(request);
        JSONObject object = new JSONObject();
        object.put("num",paging.getTotal());
        return object;
    }

    @ApiOperation("2、获取报警列表")
    @GetMapping("/v1/getAlarmList")
    @SaCheckLogin
    public JSONArray getAlarmList() {
        PageRequest<AlertRecord> request = new PageRequest<>();
        AlertRecord alertRecord = new AlertRecord();
        alertRecord.setReadFlg(false);
        request.setData(alertRecord);
        request.setPageNum(1);
        request.setPageSize(10000);
        Paging<AlertRecord> paging = alertService.selectAlertRecordPage(request);
        JSONArray jsonArray = new JSONArray();
        for (AlertRecord record:paging.getRows()){
            JSONObject object = new JSONObject();
            object.put("id",record.getId());
            object.put("deviceName",record.getDeviceName());
            object.put("deviceId",record.getDeviceId());
            object.put("floor",record.getFloor());
            object.put("name",record.getName());
            object.put("time", DateUtil.formatDateTime(new Date(record.getAlertTime())));
            JSONObject type = new JSONObject();
            type.put("name",record.getAlarmTypeName());
            type.put("value",record.getAlarmType());
            object.put("type",type);
            jsonArray.add(object);
        }
        return jsonArray;
    }

    @ApiOperation("3、保养提醒的处置")
    @PostMapping("/v1/dispose")
    @SaCheckLogin
    public JSONObject dispose(@RequestBody @Validated Request<JSONObject> objectRequest) {
        Long id = objectRequest.getData().getLong("id");
        AlertRecord alertRecord = alertService.getDetail(id);
        PageRequest<AlertRecord> request = new PageRequest<>();
        request.setPageSize(999);
        request.setPageNum(1);
        alertRecord.setReadFlg(false);
        request.setData(alertRecord);
        Paging<AlertRecord> alertRecordPaging = alertService.selectAlertRecordPage(request);
        if (alertRecordPaging!=null && alertRecordPaging.getRows()!=null && alertRecordPaging.getRows().size()>0){
            List<AlertRecord> alertRecords = alertRecordPaging.getRows();
            for (AlertRecord record:alertRecords){
                record.setReadFlg(true);
                alertService.updateAlert(record);
            }
        }
        //设备下次保养数据归零
        if (alertRecord.getDeviceId()!=null) {
            DeviceInfo deviceInfo = deviceInfoDataImpl.findByDeviceId(alertRecord.getDeviceId());
            if(deviceInfo!=null){
                ProductVo product = productService.findByProductKey(deviceInfo.getProductKey());
                if (product!=null && product.getMaintainCycle()!=null && product.getMaintainCycle().compareTo(Decimal.ZERO)>0) {
                    // 获取当前日期
                    Date currentDate = new Date();
                    // 使用Calendar来添加天数
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(currentDate);
                    calendar.add(Calendar.DATE, product.getMaintainCycle().intValue()); // 将日期向后推N天
                    // 从Calendar获取新的日期
                    Date tenDaysLater = calendar.getTime();
                    deviceInfo.setNextMaintainTime(tenDaysLater);
                }
                deviceInfo.setRunningHours(0l);
                deviceInfoDataImpl.save(deviceInfo);
            }
        }
        return null;
    }
}
