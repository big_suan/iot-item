package cc.iotkit.openapi.controller;

import cc.iotkit.common.api.PageRequest;
import cc.iotkit.common.api.Paging;
import cc.iotkit.common.api.Request;
import cc.iotkit.common.constant.Constants;
import cc.iotkit.data.manager.IDeviceInfoData;
import cc.iotkit.manager.dto.bo.device.DeviceQueryBo;
import cc.iotkit.manager.dto.bo.devicegroup.DeviceGroupBo;
import cc.iotkit.manager.dto.vo.category.CategoryVo;
import cc.iotkit.manager.dto.vo.devicegroup.DeviceGroupVo;
import cc.iotkit.manager.dto.vo.deviceinfo.DeviceInfoVo;
import cc.iotkit.manager.dto.vo.product.ProductVo;
import cc.iotkit.manager.dto.vo.record.DoorAccessRecordVo;
import cc.iotkit.manager.dto.vo.thingmodel.ThingModelVo;
import cc.iotkit.manager.service.DoorAccessRecordService;
import cc.iotkit.manager.service.IDeviceManagerService;
import cc.iotkit.manager.service.IProductService;
import cc.iotkit.model.device.DeviceInfo;
import cc.iotkit.model.device.message.DevicePropertyCache;
import cc.iotkit.model.product.Product;
import cc.iotkit.model.product.ThingModel;
import cc.iotkit.model.report.DoorAccessRecord;
import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Api(tags = {"openapi-监控中心"})
@Slf4j
@RestController
@RequestMapping("/openapi/monitor")
public class MonitorOpenApi {

    @Autowired
    private IDeviceManagerService deviceServiceImpl;
    @Autowired
    @Qualifier("deviceInfoDataCache")
    private IDeviceInfoData deviceInfoData;
    @Autowired
    DoorAccessRecordService doorAccessRecordService;

    @Autowired
    private IProductService productService;
    @ApiOperation("1、获取位置列表")
    @GetMapping("/v1/getClasses")
    @SaCheckLogin
    public JSONArray runStatistics() {
        JSONArray jsonArray = new JSONArray();
        JSONObject object = new JSONObject();
        object.put("id","0");
        object.put("no",0);
        object.put("name","jk_sw_");
        object.put("title","室外");
        jsonArray.add(object);

        object = new JSONObject();
        object.put("id","1");
        object.put("no",1);
        object.put("name","jk_1f_");
        object.put("title","一层");
        jsonArray.add(object);

        object = new JSONObject();
        object.put("id","2");
        object.put("no",2);
        object.put("name","jk_2f_");
        object.put("title","二层");
        jsonArray.add(object);

        object = new JSONObject();
        object.put("id","3");
        object.put("no",3);
        object.put("name","jk_ld_");
        object.put("title","楼顶");
        jsonArray.add(object);

        return jsonArray;
    }

    @ApiOperation("2、获取产品列表")
    @PostMapping("/v1/getProductList")
    @SaCheckLogin
    public JSONArray getProductList(@RequestBody @Validated Request<JSONObject> objectRequest) {
        JSONObject requestObj = objectRequest.getData();
        String flag = requestObj.getString("flag");
        JSONArray returnArr = new JSONArray();
        PageRequest<DeviceQueryBo> pageRequest = new PageRequest<>();
        DeviceQueryBo deviceQueryBo = new DeviceQueryBo();
        deviceQueryBo.setFloor(flag);
        pageRequest.setPageSize(99);
        pageRequest.setPageNum(1);
        pageRequest.setData(deviceQueryBo);
        Paging<DeviceInfoVo> deviceInfoVoPaging = deviceServiceImpl.getDevices(pageRequest);
        if (deviceInfoVoPaging.getRows()!=null && deviceInfoVoPaging.getRows().size()>0){
            //存放品类JSONObj
            JSONObject Air = null;
            JSONObject Blower = null;
            JSONObject Camera = null;
            JSONObject door = null;
            JSONObject Electrical_MI = null;
            JSONObject gas_warning = null;
            JSONObject gateway = null;
            JSONObject humiture = null;
            JSONObject new_trend = null;
            JSONObject pfj = null;
            JSONObject pfj1 = null;
            JSONObject pressure = null;

            //存放品类下产品集合
            JSONArray Air_ARR = null;
            JSONArray Blower_ARR = null;
            JSONArray Camera_ARR = null;
            JSONArray door_ARR = null;
            JSONArray Electrical_MI_ARR = null;
            JSONArray gas_warning_ARR = null;
            JSONArray gateway_ARR = null;
            JSONArray humiture_ARR = null;
            JSONArray new_trend_ARR = null;
            JSONArray pfj_ARR = null;
            JSONArray pfj_ARR1 = null;
            JSONArray pressure_ARR = null;

            List<DeviceInfoVo> deviceInfoVoList = deviceInfoVoPaging.getRows();
            DeviceInfoVo deviceInfoVo = null;
            JSONObject chanpinJSON = null;

            //存放排风系统产品
            JSONObject bflfJSON = null;
            JSONObject mflJSON = null;
            JSONObject wxzJSON = null;
            JSONObject tfgJSON = null;
            JSONObject pfjJSON = null;
            JSONObject pfjJSON1 = null;

            //气体检测
            JSONObject yqJSON = null;
            JSONObject krqtJSON = null;

            //新风系统
            JSONObject ffuJSON = null;
            JSONObject sfJSON = null;
            JSONObject hfJSON = null;
            JSONObject xfjzJSON = null;


            for (int i=0;i<deviceInfoVoList.size();i++){
                deviceInfoVo = deviceInfoVoList.get(i);
                String pinleiCode = deviceInfoVo.getProduct().getCategory();
                if (Constants.AIR.equals(pinleiCode)){
                    if(Air==null){
                        Air = new JSONObject();
                        Air.put("id",Constants.AIR);
                        Air.put("categoriesName",Constants.AIR_NAME);
                    }
                    if(Air_ARR==null){
                        Air_ARR = new JSONArray();
                    }
                    if (Air_ARR.size()==0) {
                        chanpinJSON = new JSONObject();
                        chanpinJSON.put("total",0);
                        chanpinJSON.put("online",0);
                        chanpinJSON.put("outline",0);
                        chanpinJSON.put("bad",0);
                        chanpinJSON.put("id",deviceInfoVo.getProductKey());
                        chanpinJSON.put("productName",deviceInfoVo.getProduct().getName());
                    }else {
                        chanpinJSON = Air_ARR.getJSONObject(0);
                    }

                    chanpinJSON.put("total",chanpinJSON.getIntValue("total")+1);

                    if (deviceInfoVo.getOnline()){
                        //查询判断当前冷热源的开机属性
//                        Map<String, DevicePropertyCache> propertyCacheMap = deviceInfoData.getProperties(deviceInfoVo.getDeviceId());
//                        if (propertyCacheMap!=null && propertyCacheMap.get("onOff")!=null && "1".equals(propertyCacheMap.get("onOff").getValue())){
                            chanpinJSON.put("online",chanpinJSON.getIntValue("online")+1);
//                        }
                    }else {
                        chanpinJSON.put("outline",chanpinJSON.getIntValue("outline")+1);
                    }
                    if (Air_ARR!=null && Air_ARR.size()==0) {
                        Air_ARR.add(chanpinJSON);
                    }
                    Air.put("list",Air_ARR);
                }
                //摄像头品类
                if (Constants.CAMERA.equals(pinleiCode)){
                    if(Camera==null){
                        Camera = new JSONObject();
                        Camera.put("id",Constants.CAMERA);
                        Camera.put("categoriesName",Constants.CAMERA_NAME);
                    }
                    if(Camera_ARR==null){
                        Camera_ARR = new JSONArray();
                    }
                    if (Camera_ARR.size()==0) {
                        chanpinJSON = new JSONObject();
                        chanpinJSON.put("total",0);
                        chanpinJSON.put("online",0);
                        chanpinJSON.put("outline",0);
                        chanpinJSON.put("bad",0);
                        chanpinJSON.put("id",deviceInfoVo.getProductKey());
                        chanpinJSON.put("productName",deviceInfoVo.getProduct().getName());
                    }else {
                        chanpinJSON = Camera_ARR.getJSONObject(0);
                    }
                    chanpinJSON.put("total",chanpinJSON.getIntValue("total")+1);
//                    if (deviceInfoVo.getOnline()){
                    //摄像头全部在线
                        chanpinJSON.put("online",chanpinJSON.getIntValue("online")+1);
//                    }else {
//                        chanpinJSON.put("outline",chanpinJSON.getIntValue("outline")+1);
//                    }
                    if (Camera_ARR!=null && Camera_ARR.size()==0) {
                        Camera_ARR.add(chanpinJSON);
                    }
                    Camera.put("list",Camera_ARR);
                }

                //门禁
                if (Constants.DOOR.equals(pinleiCode)){
                    if(door==null){
                        door = new JSONObject();
                        door.put("id",Constants.DOOR);
                        door.put("categoriesName",Constants.DOOR_NAME);
                    }
                    if(door_ARR==null){
                        door_ARR = new JSONArray();
                    }
                    if (door_ARR.size()==0) {
                        chanpinJSON = new JSONObject();
                        chanpinJSON.put("total",0);
                        chanpinJSON.put("online",0);
                        chanpinJSON.put("outline",0);
                        chanpinJSON.put("bad",0);
                        chanpinJSON.put("id",deviceInfoVo.getProductKey());
                        chanpinJSON.put("productName",deviceInfoVo.getProduct().getName());
                    }else {
                        chanpinJSON = door_ARR.getJSONObject(0);
                    }
                    chanpinJSON.put("total",chanpinJSON.getIntValue("total")+1);

//                    if (deviceInfoVo.getOnline()){
                        //全部在线
                        chanpinJSON.put("online",chanpinJSON.getIntValue("online")+1);
//                    }else {
//                        chanpinJSON.put("outline",chanpinJSON.getIntValue("outline")+1);
//                    }
                    if (door_ARR!=null && door_ARR.size()==0) {
                        door_ARR.add(chanpinJSON);
                    }
                    door.put("list",door_ARR);
                }

                //电表
                if (Constants.ELECTRICAL_MI.equals(pinleiCode)){
                    if(Electrical_MI==null){
                        Electrical_MI = new JSONObject();
                        Electrical_MI.put("id",Constants.ELECTRICAL_MI);
                        Electrical_MI.put("categoriesName",Constants.ELECTRICAL_MI_NAME);
                    }
                    if(Electrical_MI_ARR==null){
                        Electrical_MI_ARR = new JSONArray();
                    }
                    if (Electrical_MI_ARR.size()==0) {
                        chanpinJSON = new JSONObject();
                        chanpinJSON.put("total",0);
                        chanpinJSON.put("online",0);
                        chanpinJSON.put("outline",0);
                        chanpinJSON.put("bad",0);
                        chanpinJSON.put("id",deviceInfoVo.getProductKey());
                        chanpinJSON.put("productName",deviceInfoVo.getProduct().getName());
                    }else {
                        chanpinJSON = Electrical_MI_ARR.getJSONObject(0);
                    }
                    chanpinJSON.put("total",chanpinJSON.getIntValue("total")+1);

                    if (deviceInfoVo.getOnline()){
                        chanpinJSON.put("online",chanpinJSON.getIntValue("online")+1);
                    }else {
                        chanpinJSON.put("outline",chanpinJSON.getIntValue("outline")+1);
                    }
                    if (Electrical_MI_ARR!=null && Electrical_MI_ARR.size()==0) {
                        Electrical_MI_ARR.add(chanpinJSON);
                    }
                    Electrical_MI.put("list",Electrical_MI_ARR);
                }

                //温湿度
                if (Constants.HUMITURE_PK.equals(deviceInfoVo.getProductKey())){
                    if(humiture==null){
                        humiture = new JSONObject();
                        humiture.put("id",Constants.HUMITURE);
                        humiture.put("categoriesName",Constants.HUMITURE_NAME);
                    }
                    if(humiture_ARR==null){
                        humiture_ARR = new JSONArray();
                    }
                    if (humiture_ARR.size()==0) {
                        chanpinJSON = new JSONObject();
                        chanpinJSON.put("total",0);
                        chanpinJSON.put("online",0);
                        chanpinJSON.put("outline",0);
                        chanpinJSON.put("bad",0);
                        chanpinJSON.put("id",deviceInfoVo.getProductKey());
                        chanpinJSON.put("productName",deviceInfoVo.getProduct().getName());
                    }else {
                        chanpinJSON = humiture_ARR.getJSONObject(0);
                    }
                    chanpinJSON.put("total",chanpinJSON.getIntValue("total")+1);

                    if (deviceInfoVo.getOnline()){
                        chanpinJSON.put("online",chanpinJSON.getIntValue("online")+1);
                    }else {
                        chanpinJSON.put("outline",chanpinJSON.getIntValue("outline")+1);
                    }
                    if (humiture_ARR!=null && humiture_ARR.size()==0) {
                        humiture_ARR.add(chanpinJSON);
                    }
                    humiture.put("list",humiture_ARR);
                }
                //排风机
                if (Constants.PFJ.equals(pinleiCode) && !"17189655941600030000000000000005a".equals(deviceInfoVo.getDeviceId())){
                    if(pfj==null){
                        pfj = new JSONObject();
                        pfj.put("id",Constants.PFJ);
                        pfj.put("categoriesName",Constants.PFJ_NAME);
                    }
                    if(pfj_ARR==null){
                        pfj_ARR = new JSONArray();
                    }
                    if (pfj_ARR.size()==0) {
                        chanpinJSON = new JSONObject();
                        chanpinJSON.put("total",0);
                        chanpinJSON.put("online",0);
                        chanpinJSON.put("outline",0);
                        chanpinJSON.put("bad",0);
                        chanpinJSON.put("id",deviceInfoVo.getProductKey());
                        chanpinJSON.put("productName",deviceInfoVo.getProduct().getName());
                    }else {
                        chanpinJSON = pfj_ARR.getJSONObject(0);
                    }
                    chanpinJSON.put("total",chanpinJSON.getIntValue("total")+1);

                    if (deviceInfoVo.getOnline()){
                        chanpinJSON.put("online",chanpinJSON.getIntValue("online")+1);
                    }else {
                        chanpinJSON.put("outline",chanpinJSON.getIntValue("outline")+1);
                    }
                    if (pfj_ARR!=null && pfj_ARR.size()==0) {
                        pfj_ARR.add(chanpinJSON);
                    }
                    pfj.put("list",pfj_ARR);
                }

                //压力
                if (Constants.PRESSURE.equals(pinleiCode)){

                    if(pressure==null){
                        pressure = new JSONObject();
                        pressure.put("id",Constants.PRESSURE);
                        pressure.put("categoriesName",Constants.PRESSURE_NAME);
                    }
                    if(pressure_ARR==null){
                        pressure_ARR = new JSONArray();
                    }
                    if (pressure_ARR.size()==0) {
                        chanpinJSON = new JSONObject();
                        chanpinJSON.put("total",0);
                        chanpinJSON.put("online",0);
                        chanpinJSON.put("outline",0);
                        chanpinJSON.put("bad",0);
                        chanpinJSON.put("id",deviceInfoVo.getProductKey());
                        chanpinJSON.put("productName",deviceInfoVo.getProduct().getName());
                    }else {
                        chanpinJSON = pressure_ARR.getJSONObject(0);
                    }
                    chanpinJSON.put("total",chanpinJSON.getIntValue("total")+1);

                    if (deviceInfoVo.getOnline()){
                        chanpinJSON.put("online",chanpinJSON.getIntValue("online")+1);
                    }else {
                        chanpinJSON.put("outline",chanpinJSON.getIntValue("outline")+1);
                    }
                    if (pressure_ARR!=null && pressure_ARR.size()==0) {
                        pressure_ARR.add(chanpinJSON);
                    }
                    pressure.put("list",pressure_ARR);
                }

                //排风系统===增加排风机3--做特殊处理
                if (Constants.BLOWER.equals(pinleiCode) || "17189655941600030000000000000005a".equals(deviceInfoVo.getDeviceId())){
                    if(Blower==null){
                        Blower = new JSONObject();
                        Blower.put("id",Constants.BLOWER);
                        Blower.put("categoriesName",Constants.BLOWER_NAME);
                    }
                    if(Blower_ARR==null){
                        Blower_ARR = new JSONArray();
                    }
                    //变风量阀
                    if (Constants.BFLF_PK.equals(deviceInfoVo.getProductKey())) {
                        if (bflfJSON == null) {
                            bflfJSON = new JSONObject();
                            bflfJSON.put("total", 0);
                            bflfJSON.put("online", 0);
                            bflfJSON.put("outline", 0);
                            bflfJSON.put("bad", 0);
                            bflfJSON.put("id", deviceInfoVo.getProductKey());
                            bflfJSON.put("productName", deviceInfoVo.getProduct().getName());
                        }
                        bflfJSON.put("total", bflfJSON.getIntValue("total") + 1);

                        if (deviceInfoVo.getOnline()) {
                            bflfJSON.put("online", bflfJSON.getIntValue("online") + 1);
                        } else {
                            bflfJSON.put("outline", bflfJSON.getIntValue("outline") + 1);
                        }
                    }

                    //马弗炉排风阀
                    if (Constants.MFL_PK.equals(deviceInfoVo.getProductKey())) {
                        if (mflJSON == null) {
                            mflJSON = new JSONObject();
                            mflJSON.put("total", 0);
                            mflJSON.put("online", 0);
                            mflJSON.put("outline", 0);
                            mflJSON.put("bad", 0);
                            mflJSON.put("id", deviceInfoVo.getProductKey());
                            mflJSON.put("productName", deviceInfoVo.getProduct().getName());
                        }
                        mflJSON.put("total", mflJSON.getIntValue("total") + 1);

                        if (deviceInfoVo.getOnline()) {
                            mflJSON.put("online", mflJSON.getIntValue("online") + 1);
                        } else {
                            mflJSON.put("outline", mflJSON.getIntValue("outline") + 1);
                        }
                    }

                    //万向罩阀门
                    if (Constants.WXZ_PK.equals(deviceInfoVo.getProductKey())) {
                        if (wxzJSON == null) {
                            wxzJSON = new JSONObject();
                            wxzJSON.put("total", 0);
                            wxzJSON.put("online", 0);
                            wxzJSON.put("outline", 0);
                            wxzJSON.put("bad", 0);
                            wxzJSON.put("id", deviceInfoVo.getProductKey());
                            wxzJSON.put("productName", deviceInfoVo.getProduct().getName());
                        }
                        wxzJSON.put("total", wxzJSON.getIntValue("total") + 1);

                        if (deviceInfoVo.getOnline()) {
                            wxzJSON.put("online", wxzJSON.getIntValue("online") + 1);
                        } else {
                            wxzJSON.put("outline", wxzJSON.getIntValue("outline") + 1);
                        }
                    }

                    //通风柜排风阀门
                    if (Constants.TFG_PK.equals(deviceInfoVo.getProductKey())) {
                        if (tfgJSON == null) {
                            tfgJSON = new JSONObject();
                            tfgJSON.put("total", 0);
                            tfgJSON.put("online", 0);
                            tfgJSON.put("outline", 0);
                            tfgJSON.put("bad", 0);
                            tfgJSON.put("id", deviceInfoVo.getProductKey());
                            tfgJSON.put("productName", deviceInfoVo.getProduct().getName());
                        }
                        tfgJSON.put("total", tfgJSON.getIntValue("total") + 1);

                        if (deviceInfoVo.getOnline()) {
                            tfgJSON.put("online", tfgJSON.getIntValue("online") + 1);
                        } else {
                            tfgJSON.put("outline", tfgJSON.getIntValue("outline") + 1);
                        }
                    }
                    //排风机03
                    if ("17189655941600030000000000000005a".equals(deviceInfoVo.getDeviceId())) {
                        if (pfjJSON1 == null) {
                            pfjJSON1 = new JSONObject();
                            pfjJSON1.put("total", 0);
                            pfjJSON1.put("online", 0);
                            pfjJSON1.put("outline", 0);
                            pfjJSON1.put("bad", 0);
                            pfjJSON1.put("id", deviceInfoVo.getProductKey());
                            pfjJSON1.put("productName", deviceInfoVo.getProduct().getName());
                        }
                        pfjJSON1.put("total", 1);

                        if (deviceInfoVo.getOnline()) {
                            pfjJSON1.put("online", pfjJSON1.getIntValue("online") + 1);
                        } else {
                            pfjJSON1.put("outline", pfjJSON1.getIntValue("outline") + 1);
                        }
                    }
                }

                //气体检测
                if (Constants.GAS_WARNING.equals(pinleiCode)){
                    if(gas_warning==null){
                        gas_warning = new JSONObject();
                        gas_warning.put("id",Constants.GAS_WARNING);
                        gas_warning.put("categoriesName",Constants.GAS_WARNING_NAME);
                    }
                    if(gas_warning_ARR==null){
                        gas_warning_ARR = new JSONArray();
                    }
                    //氧气
                    if (Constants.YQ_PK.equals(deviceInfoVo.getProductKey())) {
                        if (yqJSON == null) {
                            yqJSON = new JSONObject();
                            yqJSON.put("total", 0);
                            yqJSON.put("online", 0);
                            yqJSON.put("outline", 0);
                            yqJSON.put("bad", 0);
                            yqJSON.put("id", deviceInfoVo.getProductKey());
                            yqJSON.put("productName", deviceInfoVo.getProduct().getName());
                        }
                        yqJSON.put("total", yqJSON.getIntValue("total") + 1);

                        if (deviceInfoVo.getOnline()) {
                            yqJSON.put("online", yqJSON.getIntValue("online") + 1);
                        } else {
                            yqJSON.put("outline", yqJSON.getIntValue("outline") + 1);
                        }
                    }

                    //可燃气体
                    if (Constants.KRQT_PK.equals(deviceInfoVo.getProductKey())) {
                        if (krqtJSON == null) {
                            krqtJSON = new JSONObject();
                            krqtJSON.put("total", 0);
                            krqtJSON.put("online", 0);
                            krqtJSON.put("outline", 0);
                            krqtJSON.put("bad", 0);
                            krqtJSON.put("id", deviceInfoVo.getProductKey());
                            krqtJSON.put("productName", deviceInfoVo.getProduct().getName());
                        }
                        krqtJSON.put("total", krqtJSON.getIntValue("total") + 1);

                        if (deviceInfoVo.getOnline()) {
                            krqtJSON.put("online", krqtJSON.getIntValue("online") + 1);
                        } else {
                            krqtJSON.put("outline", krqtJSON.getIntValue("outline") + 1);
                        }
                    }
                }

                //新风系统
                if (Constants.NEW_TREND.equals(pinleiCode)){
                    if(new_trend==null){
                        new_trend = new JSONObject();
                        new_trend.put("id",Constants.NEW_TREND);
                        new_trend.put("categoriesName",Constants.NEW_TREND_NAME);
                    }
                    if(new_trend_ARR==null){
                        new_trend_ARR = new JSONArray();
                    }
                    //FFU
                    if (Constants.FFU_PK.equals(deviceInfoVo.getProductKey())) {
                        if (ffuJSON == null) {
                            ffuJSON = new JSONObject();
                            ffuJSON.put("total", 0);
                            ffuJSON.put("online", 0);
                            ffuJSON.put("outline", 0);
                            ffuJSON.put("bad", 0);
                            ffuJSON.put("id", deviceInfoVo.getProductKey());
                            ffuJSON.put("productName", deviceInfoVo.getProduct().getName());
                        }
                        ffuJSON.put("total", ffuJSON.getIntValue("total") + 1);

                        if (deviceInfoVo.getOnline()) {
                            ffuJSON.put("online", ffuJSON.getIntValue("online") + 1);
                        } else {
                            ffuJSON.put("outline", ffuJSON.getIntValue("outline") + 1);
                        }
                    }

                    //送风阀门
                    if (Constants.SF_PK.equals(deviceInfoVo.getProductKey())) {
                        if (sfJSON == null) {
                            sfJSON = new JSONObject();
                            sfJSON.put("total", 0);
                            sfJSON.put("online", 0);
                            sfJSON.put("outline", 0);
                            sfJSON.put("bad", 0);
                            sfJSON.put("id", deviceInfoVo.getProductKey());
                            sfJSON.put("productName", deviceInfoVo.getProduct().getName());
                        }
                        sfJSON.put("total", sfJSON.getIntValue("total") + 1);

                        if (deviceInfoVo.getOnline()) {
                            sfJSON.put("online", sfJSON.getIntValue("online") + 1);
                        } else {
                            sfJSON.put("outline", sfJSON.getIntValue("outline") + 1);
                        }
                    }

                    //回风阀门
                    if (Constants.HF_PK.equals(deviceInfoVo.getProductKey())) {
                        if (hfJSON == null) {
                            hfJSON = new JSONObject();
                            hfJSON.put("total", 0);
                            hfJSON.put("online", 0);
                            hfJSON.put("outline", 0);
                            hfJSON.put("bad", 0);
                            hfJSON.put("id", deviceInfoVo.getProductKey());
                            hfJSON.put("productName", deviceInfoVo.getProduct().getName());
                        }
                        hfJSON.put("total", hfJSON.getIntValue("total") + 1);

                        if (deviceInfoVo.getOnline()) {
                            hfJSON.put("online", hfJSON.getIntValue("online") + 1);
                        } else {
                            hfJSON.put("outline", hfJSON.getIntValue("outline") + 1);
                        }
                    }
                    //新风系统机组
                    if (Constants.XFJZ_PK.equals(deviceInfoVo.getProductKey())) {
                        if (xfjzJSON == null) {
                            xfjzJSON = new JSONObject();
                            xfjzJSON.put("total", 0);
                            xfjzJSON.put("online", 0);
                            xfjzJSON.put("outline", 0);
                            xfjzJSON.put("bad", 0);
                            xfjzJSON.put("id", deviceInfoVo.getProductKey());
                            xfjzJSON.put("productName", deviceInfoVo.getProduct().getName());
                        }
                        xfjzJSON.put("total", xfjzJSON.getIntValue("total") + 1);

                        if (deviceInfoVo.getOnline()) {
                            xfjzJSON.put("online", xfjzJSON.getIntValue("online") + 1);
                        } else {
                            xfjzJSON.put("outline", xfjzJSON.getIntValue("outline") + 1);
                        }
                    }
                }
            }

            if (Air!=null){
                returnArr.add(Air);
            }
            if (Camera!=null){
                returnArr.add(Camera);
            }
            if (door!=null){
                returnArr.add(door);
            }
            if (humiture!=null){
                returnArr.add(humiture);
            }
            if (Electrical_MI!=null){
                returnArr.add(Electrical_MI);
            }
            if (pfj!=null){
                returnArr.add(pfj);
            }
            if (pressure!=null){
                returnArr.add(pressure);
            }
//////////======================================排风系统=========================start
            if (Blower_ARR != null) {
                if(bflfJSON!=null) {
                    Blower_ARR.add(bflfJSON);
                }
                if(mflJSON!=null) {
                    Blower_ARR.add(mflJSON);
                }
                if(wxzJSON!=null) {
                    Blower_ARR.add(wxzJSON);
                }
                if(tfgJSON!=null) {
                    Blower_ARR.add(tfgJSON);
                }
                if (pfjJSON1!=null){
                    Blower_ARR.add(pfjJSON1);
                }
                Blower.put("list",Blower_ARR);
            }
            if (Blower!=null){
                returnArr.add(Blower);
            }
//////////======================================排风系统=========================end
//////////////=============================气体检测===============
            if (gas_warning_ARR != null) {
                if(yqJSON!=null) {
                    gas_warning_ARR.add(yqJSON);
                }
                if(krqtJSON!=null) {
                    gas_warning_ARR.add(krqtJSON);
                }
                gas_warning.put("list",gas_warning_ARR);
            }
            if (gas_warning!=null){
                returnArr.add(gas_warning);
            }
////////////-============================气体检测=====================
////////////-============================新风系统=======================start
            if (new_trend_ARR!=null) {
                if (ffuJSON != null) {
                    new_trend_ARR.add(ffuJSON);
                }
                if (sfJSON != null) {
                    new_trend_ARR.add(sfJSON);
                }
                if (hfJSON != null) {
                    new_trend_ARR.add(hfJSON);
                }
                if (xfjzJSON != null) {
                    new_trend_ARR.add(xfjzJSON);
                }
                new_trend.put("list", new_trend_ARR);
            }
            if (new_trend!=null){
                returnArr.add(new_trend);
            }
////////////-============================新风系统=======================end

        }
        return returnArr;
    }

    @ApiOperation("3、获取设备详情")
    @PostMapping("/v1/deviceDetails")
    @SaCheckLogin
    public JSONObject deviceDetails(@RequestBody @Validated Request<JSONObject> objectRequest) {
        if (objectRequest.getData().get("deviceId")==null){
            return null;
        }
        JSONObject returnObj = new JSONObject();
        String deviceId = objectRequest.getData().getString("deviceId");
        DeviceInfo deviceInfo = deviceServiceImpl.getDetail(deviceId);
        //判断当前设备类型 做不同处理 摄像头 只返回基本信息，门禁 需要返回近24小时的出入记录；其他设备返回当前设备的物模型数据
        if (deviceInfo!=null) {
            ProductVo productVo = productService.getDetail(deviceInfo.getProductKey());
            returnObj.put("categoryCode",productVo.getCategory());
            returnObj.put("deviceId",deviceId);
            JSONArray baseInfo = new JSONArray();
            JSONObject sonObj = new JSONObject();

            JSONArray assetInfo = new JSONArray();
            if (deviceInfo.getManufacturers()!=null) {
                sonObj.put("name", "生产厂家");
                sonObj.put("value", deviceInfo.getManufacturers().getName());
                assetInfo.add(sonObj);
                sonObj = new JSONObject();
                sonObj.put("name","联系电话");
                sonObj.put("value",deviceInfo.getManufacturers().getLinkPhone());
                assetInfo.add(sonObj);
            }

            if (deviceInfo.getSupplier()!=null) {
                sonObj = new JSONObject();
                sonObj.put("name", "供货厂家");
                sonObj.put("value", deviceInfo.getSupplier().getName());
                assetInfo.add(sonObj);

                sonObj = new JSONObject();
                sonObj.put("name", "联系电话");
                sonObj.put("value", deviceInfo.getSupplier().getLinkPhone());
                assetInfo.add(sonObj);
            }

            sonObj = new JSONObject();
            sonObj.put("name","购置时间");
            sonObj.put("value",deviceInfo.getBugTime());
            assetInfo.add(sonObj);

            returnObj.put("assetInfo",assetInfo);

            JSONArray realtime = new JSONArray();

            if (Constants.CAMERA_PK.equals(deviceInfo.getProductKey())){
                sonObj = new JSONObject();
                sonObj.put("deviceId",deviceId);
                baseInfo.add(sonObj);
            }else if (Constants.DOOR_PK.equals(deviceInfo.getProductKey())){
                sonObj = new JSONObject();
                sonObj.put("name","设备名称");
                sonObj.put("value",deviceInfo.getDeviceName());
                baseInfo.add(sonObj);

                sonObj = new JSONObject();
                sonObj.put("name","运行状态");
                sonObj.put("value","在线");
                baseInfo.add(sonObj);
                //出入记录
                PageRequest<DoorAccessRecord> query = new PageRequest<>();
                DoorAccessRecord doorAccessRecord = new DoorAccessRecord();
                doorAccessRecord.setDeviceName(deviceInfo.getDeviceName());
                query.setData(doorAccessRecord);
                Map map = new HashMap();
                map.put("dateTime", "DESC");
                query.setSortMap(map);
                Paging<DoorAccessRecordVo> doorAccessRecordVoPaging = doorAccessRecordService.selectPageDoorAccessRecordList(query);
                if (doorAccessRecordVoPaging.getRows()!=null && doorAccessRecordVoPaging.getRows().size()>0){
                    List<DoorAccessRecordVo> doorAccessRecordVoList = doorAccessRecordVoPaging.getRows();
                    JSONObject renyuan = null;
                    for (DoorAccessRecordVo doorAccessRecordVo:doorAccessRecordVoList){
                        renyuan = new JSONObject();
                        renyuan.put("employeeNo",doorAccessRecordVo.getEmployeeNo());
                        renyuan.put("employeeName",doorAccessRecordVo.getEmployeeName());
                        renyuan.put("dateTime",DateUtil.formatDateTime(doorAccessRecordVo.getDateTime()));
                        realtime.add(renyuan);
                    }
                }
                returnObj.put("realtime",realtime);
            }else {
                sonObj = new JSONObject();
                sonObj.put("name","设备名称");
                sonObj.put("value",deviceInfo.getDeviceName());
                baseInfo.add(sonObj);

                sonObj = new JSONObject();
                sonObj.put("name","运行状态");
                sonObj.put("value",deviceInfo.isOnline()?"在线":"离线");
                baseInfo.add(sonObj);
                //获取当前产品的物模型，==给当前设备封装物模型值
                ThingModelVo thingModelVo = productService.getThingModelByProductKey(deviceInfo.getProductKey());
                List<ThingModel.Property> propertyList = thingModelVo.getModel().getProperties();
                Map<String, DevicePropertyCache> properties = (Map<String, DevicePropertyCache>) deviceInfo.getProperty();
                Set<String> keys = properties.keySet();
                for (String key:keys) {
                    JSONObject realData = null;
                    for (ThingModel.Property property:propertyList) {
                        if (property.getIdentifier().equals(key)) {
                            realData = new JSONObject();
                            realData.put("name", property.getName());
                            String value = properties.get(key).getValue()!=null ? properties.get(key).getValue()+"":"";
                            if ("onOff".equals(property.getIdentifier())){
                                if ("1".equals(value)){
                                    value="开机";
                                }else {
                                    value="关机";
                                }
                            }else if ("runningMode".equals(property.getIdentifier())){
                                if ("0".equals(value)){
                                    value="制冷模式";
                                }else if("1".equals(value)) {
                                    value="制热模式";
                                }else if("2".equals(value)) {
                                    value="通风模式";
                                }
                            }else if ("breakdown".equals(property.getIdentifier())){
                                if ("0".equals(value)){
                                    value="无";
                                }else if("1".equals(value)) {
                                    value="有";
                                }
                            }else if ("lowWindSpeed".equals(property.getIdentifier())){
                                if ("0".equals(value)){
                                    value="正常";
                                }else if("1".equals(value)) {
                                    value="报警";
                                }
                            }else if ("highWindSpeed".equals(property.getIdentifier())){
                                if ("0".equals(value)){
                                    value="正常";
                                }else if("1".equals(value)) {
                                    value="报警";
                                }
                            }else if ("call1Level1".equals(property.getIdentifier())){
                                if ("1".equals(value)){
                                    value="有一级报警";
                                }else {
                                    value="无一级报警";
                                }
                            }else if ("call1Level2".equals(property.getIdentifier())){
                                if ("1".equals(value)){
                                    value="有二级报警";
                                }else {
                                    value="无二级报警";
                                }
                            }else if ("malfunction1".equals(property.getIdentifier())){
                                if ("1".equals(value)){
                                    value="有故障";
                                }else {
                                    value="无故障";
                                }
                            }else if ("call2Level1".equals(property.getIdentifier())){
                                if ("1".equals(value)){
                                    value="有一级报警";
                                }else {
                                    value="无一级报警";
                                }
                            }else if ("call2Level2".equals(property.getIdentifier())){
                                if ("1".equals(value)){
                                    value="有二级报警";
                                }else {
                                    value="无二级报警";
                                }
                            }else if ("malfunction2".equals(property.getIdentifier())){
                                if ("1".equals(value)){
                                    value="有故障";
                                }else {
                                    value="无故障";
                                }
                            }else if ("nfjgz".equals(property.getIdentifier())){
                                if ("1".equals(value)){
                                    value="有故障";
                                }else {
                                    value="无故障";
                                }
                            }else if ("djryqt".equals(property.getIdentifier())){
                                if ("1".equals(value)){
                                    value="开启";
                                }else {
                                    value="关闭";
                                }
                            }else if ("jsqqt".equals(property.getIdentifier())){
                                if ("1".equals(value)){
                                    value="开启";
                                }else {
                                    value="关闭";
                                }
                            }else if ("jzgzzsd".equals(property.getIdentifier())){
                                if ("1".equals(value)){
                                    value="开启";
                                }else {
                                    value="关闭";
                                }
                            }else if ("cxzt".equals(property.getIdentifier())){
                                if ("1".equals(value)){
                                    value="报警";
                                }else {
                                    value="正常";
                                }
                            }else if ("zxzt".equals(property.getIdentifier())){
                                if ("1".equals(value)){
                                    value="报警";
                                }else {
                                    value="正常";
                                }
                            }else if ("sfjbpqgz".equals(property.getIdentifier())){
                                if ("1".equals(value)){
                                    value="报警";
                                }else {
                                    value="正常";
                                }
                            }else if ("sfjqf".equals(property.getIdentifier())){
                                if ("1".equals(value)){
                                    value="报警";
                                }else {
                                    value="正常";
                                }
                            }else if ("djrygrbh".equals(property.getIdentifier())){
                                if ("1".equals(value)){
                                    value="报警";
                                }else {
                                    value="正常";
                                }
                            }else if ("djregrbh".equals(property.getIdentifier())){
                                if ("1".equals(value)){
                                    value="报警";
                                }else {
                                    value="正常";
                                }
                            }else if ("djreqt".equals(property.getIdentifier())){
                                if ("1".equals(value)){
                                    value="开启";
                                }else {
                                    value="关闭";
                                }
                            }else if ("jzyxzsd".equals(property.getIdentifier())){
                                if ("1".equals(value)){
                                    value="开启";
                                }else {
                                    value="关闭";
                                }
                            }else if ("sfjqt".equals(property.getIdentifier())){
                                if ("1".equals(value)){
                                    value="开启";
                                }else {
                                    value="关闭";
                                }
                            }else if ("fgdjrqt".equals(property.getIdentifier())){
                                if ("1".equals(value)){
                                    value="开启";
                                }else {
                                    value="关闭";
                                }
                            }


                            if ("0.0".equals(value+"")){
                                value = "0";
                            }
                            realData.put("value",value);
                            realData.put("unit",property.getUnit());
                            break;
                        }
                    }
                    if (realData!=null) {
                        realtime.add(realData);
                    }
                }
                returnObj.put("realtime",realtime);
            }
            returnObj.put("baseInfo",baseInfo);
        }

        return returnObj;
    }

    @ApiOperation("4、获取房间信息-新增")
    @GetMapping("/v1/getRooms/{floorId}")
    @SaCheckLogin
    public JSONObject getRooms(@PathVariable String floorId) {
        JSONObject returnJSON = new JSONObject();
        PageRequest<DeviceGroupBo> pageRequest = new PageRequest<>();
        DeviceGroupBo deviceGroupBo = new DeviceGroupBo();
        deviceGroupBo.setId(floorId);
        pageRequest.setData(deviceGroupBo);
        pageRequest.setPageSize(999);
        pageRequest.setPageNum(1);
        Paging<DeviceGroupVo> deviceGroupVoPaging = deviceServiceImpl.selectGroupPageListByIdLike(pageRequest);
        if (deviceGroupVoPaging.getRows()!=null && deviceGroupVoPaging.getRows().size()>0){
            List<DeviceGroupVo> deviceGroupVoList = deviceGroupVoPaging.getRows();
            JSONArray roomArray = new JSONArray();
            JSONObject roomJSON = null;
            returnJSON.put("floorId",floorId);
            for (DeviceGroupVo deviceGroupVo:deviceGroupVoList) {
                roomJSON = new JSONObject();
                roomJSON.put("id",deviceGroupVo.getId());
                roomJSON.put("title",deviceGroupVo.getName());
                roomArray.add(roomJSON);
            }
            returnJSON.put("rooms",roomArray);
        }
        return returnJSON;
    }

    @ApiOperation("5、获取所有设备数据-树结构返回-新增")
    @GetMapping("/v1/getDeviceTree")
    @SaCheckLogin
    public JSONArray getDeviceTree() {
        JSONArray jsonArray = new JSONArray();
        //先查品类
        List<CategoryVo> categoryVoList = productService.selectCategoryList();
        //根据品类查产品
        if (categoryVoList!=null && categoryVoList.size()>0){
            JSONObject cjo = null;
            for (CategoryVo categoryVo:categoryVoList){
                if ("gateway".equals(categoryVo.getId())){
                    continue;
                }
                cjo = new JSONObject();
                cjo.put("title",categoryVo.getName());
                cjo.put("key",categoryVo.getId());
                JSONArray c_children = new JSONArray();
                List<Product> productList = productService.selectListByCategory(categoryVo.getId());
                if (productList!=null && productList.size()>0){
                    JSONObject pjo = null;
                    JSONArray p_children = null;
                    for (Product product:productList){
                        //获取产品下设备列表
                        List<DeviceInfo> deviceInfoList = deviceServiceImpl.findByPk(product.getProductKey());
                        if (deviceInfoList!=null && deviceInfoList.size()>0){
                            p_children = new JSONArray();
                            pjo = new JSONObject();
                            pjo.put("title",product.getName());
                            pjo.put("key",product.getProductKey());
                            JSONObject djo = null;
                            for (DeviceInfo deviceInfo:deviceInfoList){
                                djo = new JSONObject();
                                djo.put("title",deviceInfo.getDeviceName());
                                djo.put("key",deviceInfo.getDeviceId());
                                p_children.add(djo);
                            }
                            pjo.put("children",p_children);
                            c_children.add(pjo);
                        }
                    }
                    cjo.put("children",c_children);
                    jsonArray.add(cjo);
                }
            }
        }
        return jsonArray;
    }
}
