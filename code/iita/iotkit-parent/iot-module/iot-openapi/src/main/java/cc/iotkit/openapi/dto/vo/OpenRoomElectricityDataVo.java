package cc.iotkit.openapi.dto.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@ApiModel(value = "OpenRoomElectricityDataVo")
@Data
public class OpenRoomElectricityDataVo {

    @ApiModelProperty(value = "一层累计用电量")
    private float totalElectricity1;

    @ApiModelProperty(value = "一层每个房间用电量")
    private List<RoomElectricity> roomElectricityList1;


    @ApiModelProperty(value = "二层累计用电量")
    private float totalElectricity2;

    @ApiModelProperty(value = "二层每个房间用电量")
    private List<RoomElectricity> roomElectricityList2;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RoomElectricity {
        @ApiModelProperty(value = "房间名称")
        private String roomName;

        @ApiModelProperty(value = "房间用电量")
        private float totalElectricity;
    }
}
