package cc.iotkit.openapi.dto.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 环境数据对象
 */
@ApiModel(value = "OpenSurroundingsVo")
@Data
public class OpenSurroundingsVo {

    @ApiModelProperty(value = "温度")
    private float temp;

    @ApiModelProperty(value = "湿度")
    private float humidity;

    @ApiModelProperty(value = "可燃气体浓度")
    private float combustibleGas;

    @ApiModelProperty(value = "氧气浓度")
    private float oxygen;
}
