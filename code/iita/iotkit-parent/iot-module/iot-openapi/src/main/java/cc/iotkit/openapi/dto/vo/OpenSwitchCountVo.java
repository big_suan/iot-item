package cc.iotkit.openapi.dto.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 设备开关统计
 */
@ApiModel(value = "OpenSwitchCountVo")
@Data
public class OpenSwitchCountVo {

    @ApiModelProperty(value = "开机设备统计")
    private int onCount;

    @ApiModelProperty(value = "关机设备统计")
    private int offCount;

    @ApiModelProperty(value = "设备总数统计")
    private int allCount;
}
