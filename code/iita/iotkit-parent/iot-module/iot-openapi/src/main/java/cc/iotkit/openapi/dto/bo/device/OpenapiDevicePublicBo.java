package cc.iotkit.openapi.dto.bo.device;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@ApiModel(value = "OpenapiDevicePublicBo")
@Data
public class OpenapiDevicePublicBo {
    private static final long serialVersionUID = -1L;

    @ApiModelProperty(value = "父级ID")
    private String parentId;

    @ApiModelProperty(value = "产品名称")
    private String deviceName;

    @NotBlank(message = "productKey不能为空")
    @ApiModelProperty(value = "产品key")
    private String productKey;

    @ApiModelProperty(value = "属性标识符")
    private String identifier;

    @ApiModelProperty(value = "开始时间")
    private Long timeStart;

    @ApiModelProperty(value = "结束时间")
    private Long timeEnd;


}
