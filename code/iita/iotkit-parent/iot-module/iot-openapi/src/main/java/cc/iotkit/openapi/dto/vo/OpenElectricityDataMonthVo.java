package cc.iotkit.openapi.dto.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value = "OpenElectricityDataMonthVo")
@Data
public class OpenElectricityDataMonthVo {

    @ApiModelProperty(value = "获取当前能耗（单位kw）")
    private float energyConsumptionNow;

    @ApiModelProperty(value = "30天累计用电（单位kw·h）")
    private float energyConsumption30D_all;

    @ApiModelProperty(value = "空调30天累计用电（单位kw·h）")
    private float energyConsumption30D_air;

    @ApiModelProperty(value = "照明24小时累计用电（单位kw·h）")
    private float energyConsumption30D_lighte;

    @ApiModelProperty(value = "FFU30天累计用电（单位kw·h）")
    private float energyConsumption30D_ffu;

    @ApiModelProperty(value = "实验区30天累计用电（单位kw·h）")
    private float energyConsumption30D_test;

}
