package cc.iotkit.openapi.dto.vo;

import cc.iotkit.common.thing.ThingModelMessage;
import cc.iotkit.model.device.DeviceInfo;
import cc.iotkit.model.device.message.DeviceProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@ApiModel(value = "OpenDeviceInfoProVo")
@Data
public class OpenDeviceInfoProVo {

    private String deviceId;

    /**
     * 产品key
     */
    private String productKey;

    private String deviceName;
    /**
     * 设备所属分组
     */
    private DeviceInfo.Group group = null;
    /**
     * 楼层
     */
    private String floor;

    @ApiModelProperty(value="设备属性")
    private List<DeviceProperty> property;

    @ApiModelProperty(value="设备属性上报记录")
    private List<ThingModelMessage> modelMessages;
}
