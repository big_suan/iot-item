package cc.iotkit.openapi.dto.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value = "OpenElectricityDataVo")
@Data
public class OpenElectricityDataDayVo {

    @ApiModelProperty(value = "获取当前能耗（单位kw）")
    private float energyConsumptionNow;

    @ApiModelProperty(value = "24小时累计用电（单位kw·h）")
    private float energyConsumption24H_all;

    @ApiModelProperty(value = "空调24小时累计用电（单位kw·h）")
    private float energyConsumption24H_air;

    @ApiModelProperty(value = "照明24小时累计用电（单位kw·h）")
    private float energyConsumption24H_lighte;

    @ApiModelProperty(value = "FFU24小时累计用电（单位kw·h）")
    private float energyConsumption24H_ffu;

    @ApiModelProperty(value = "实验区24小时累计用电（单位kw·h）")
    private float energyConsumption24H_test;

}
