package cc.iotkit.openapi.dto.vo;

import cc.iotkit.manager.dto.vo.record.DoorAccessRecordVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@ApiModel(value = "OpenSecurityDataVo")
@Data
public class OpenSecurityDataVo {
    @ApiModelProperty(value = "摄像头设备数量")
    private int cameraCount;

    @ApiModelProperty(value = "门禁设备数量")
    private int accessControl;

    @ApiModelProperty(value = "人员出入门禁数据")
    private List<DoorAccessRecordVo> doorAccessRecordVoList;
}
