package cc.iotkit.openapi.dto.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value = "OpenDeviceStatusCountVo")
@Data
public class OpenDeviceStatusCountVo {
    @ApiModelProperty(value = "设备在线数量")
    private Integer onlineTotal=0;

    @ApiModelProperty(value = "设备离线数量")
    private Integer offlineTotal=0;

    @ApiModelProperty(value = "设备告警数量")
    private Integer badTotal=0;

}
