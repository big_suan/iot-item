package cc.iotkit.web.controller;

import cc.iotkit.common.api.PageRequest;
import cc.iotkit.common.api.Paging;
import cc.iotkit.common.api.Request;
import cc.iotkit.common.thing.ThingModelMessage;
import cc.iotkit.data.manager.IDeviceInfoData;
import cc.iotkit.manager.dto.bo.device.DeviceInfoBo;
import cc.iotkit.manager.dto.bo.device.DeviceLogQueryBo;
import cc.iotkit.manager.dto.bo.device.DevicePropertyLogQueryBo;
import cc.iotkit.manager.dto.bo.device.DeviceQueryBo;
import cc.iotkit.manager.dto.vo.deviceinfo.DeviceInfoVo;
import cc.iotkit.manager.service.IDeviceManagerService;
import cc.iotkit.model.device.message.DeviceProperty;
import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaIgnore;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@Api(tags = "给三方平台提供接口支撑")
@SaIgnore
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/openData")
public class OpenDataApi {
    @Autowired
    private IDeviceManagerService deviceServiceImpl;
    @Autowired
    @Qualifier("deviceInfoDataCache")
    private IDeviceInfoData deviceInfoData;

    @ApiOperation(value = "属性获取", notes = "属性获取", httpMethod = "POST")
    @SaCheckPermission("iot:device:ctrl")
    @PostMapping("/deviceProperty/new")
    public Map devicePropertyNew(@RequestBody @Validated Request<DeviceInfoBo> bo) {
        return deviceServiceImpl.getDetail(bo.getData().getDeviceId()).getProperty();
    }

    @ApiOperation("获取设备属性历史数据")
    @PostMapping("/deviceProperty/historyList")
    public List<DeviceProperty> getPropertyHistory(@RequestBody
                                                   Request<DevicePropertyLogQueryBo> query) {
        DevicePropertyLogQueryBo data = query.getData();
        String deviceId = data.getDeviceId();
        String name = data.getName();
        long start = data.getStart();
        long end = data.getEnd();
        return deviceServiceImpl.getPropertyHistory(deviceId, name, start, end, 10000);
    }
    @ApiOperation("获取设备列表")
    @PostMapping("/device/list")
    public Paging<DeviceInfoVo> getDeviceList(@Validated @RequestBody PageRequest<DeviceQueryBo> pageRequest) {
        return deviceServiceImpl.getDevices(pageRequest);
    }

    @ApiOperation("获取设备离线记录-全部-最多10000条")
    @PostMapping("/deviceOffline/historyListAll")
    public Paging<ThingModelMessage> getOfflineHistoryAll(@Validated @RequestBody PageRequest<DeviceLogQueryBo> request) {
        //设置只查询离线标识
        request.getData().setIdentifier("offline");
        request.setPageSize(10000);
        //设置只查询状态历史日志数据
        request.getData().setType("state");
        return deviceServiceImpl.logs(request);
    }

    @ApiOperation("获取设备离线记录-分页")
    @PostMapping("/deviceOffline/historyListPage")
    public Paging<ThingModelMessage> getOfflineHistoryPage(@Validated @RequestBody PageRequest<DeviceLogQueryBo> request) {
        //设置只查询离线标识
        request.getData().setIdentifier("offline");
        //设置只查询状态历史日志数据
        request.getData().setType("state");
        return deviceServiceImpl.logs(request);
    }
}
