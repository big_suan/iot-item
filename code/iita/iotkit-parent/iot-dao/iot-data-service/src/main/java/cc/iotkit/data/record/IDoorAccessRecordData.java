package cc.iotkit.data.record;

import cc.iotkit.data.ICommonData;
import cc.iotkit.model.report.DoorAccessRecord;

import java.util.List;

public interface IDoorAccessRecordData extends ICommonData<DoorAccessRecord, Long> {


    List<DoorAccessRecord> selectDoorAccessRecordList(DoorAccessRecord convert);
}
