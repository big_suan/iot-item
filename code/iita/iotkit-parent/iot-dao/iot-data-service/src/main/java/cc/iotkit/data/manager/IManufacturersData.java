
package cc.iotkit.data.manager;

import cc.iotkit.common.api.Paging;
import cc.iotkit.data.IOwnedData;
import cc.iotkit.model.device.DeviceGroup;
import cc.iotkit.model.device.DeviceInfo;
import cc.iotkit.model.device.Manufacturers;
import cc.iotkit.model.device.message.DevicePropertyCache;
import cc.iotkit.model.stats.DataItem;

import java.util.List;
import java.util.Map;

public interface IManufacturersData extends IOwnedData<Manufacturers, String> {




    /**
     * 根据Name查找
     */
    Paging<Manufacturers>  findByName(String name, int page, int size);

    /**
     * 按条件搜索设备
     *
     * @param page 页码
     * @param size 分页大小
     */
    Paging<Manufacturers> findByConditions(String uid, int page, int size);


}
