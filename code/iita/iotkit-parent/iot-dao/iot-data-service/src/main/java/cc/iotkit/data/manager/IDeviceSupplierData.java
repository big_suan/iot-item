 
package cc.iotkit.data.manager;

import cc.iotkit.common.api.Paging;
import cc.iotkit.data.IOwnedData;
import cc.iotkit.model.device.DeviceSupplier;
import cc.iotkit.model.device.Manufacturers;

public interface IDeviceSupplierData extends IOwnedData<DeviceSupplier, String> {




    /**
     * 根据Name查找
     */
    Paging<DeviceSupplier>  findByName(String name, int page, int size);

    /**
     * 按条件搜索设备
     *
     * @param page 页码
     * @param size 分页大小
     */
    Paging<DeviceSupplier> findByConditions(String uid, int page, int size);


}
