package cc.iotkit.data.model;

import cc.iotkit.model.report.DoorAccessRecord;
import io.github.linpeilie.annotations.AutoMapper;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "device_door_access_record ")
@ApiModel(value = "设备-门禁-访问记录")
@AutoMapper(target = DoorAccessRecord.class)
public class TbDoorAccessRecord {

    @Id
    @GeneratedValue(generator = "SnowflakeIdGenerator")
    @GenericGenerator(name = "SnowflakeIdGenerator", strategy = "cc.iotkit.data.config.id.SnowflakeIdGenerator")
    private Long id;

    private String deviceId;

    private String deviceName;

    private String employeeNo;

    private String employeeName;

    private Date dateTime;

    private String userType;



}
