package cc.iotkit.data.model;

import cc.iotkit.model.device.Manufacturers;
import io.github.linpeilie.annotations.AutoMapper;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Data
@Entity
@Table(name = "manufacturers ")
@ApiModel(value = "生产厂家")
@AutoMapper(target = Manufacturers.class)
public class TbManufacturers {

    @Id
    @GeneratedValue(generator = "SnowflakeIdGenerator")
    @GenericGenerator(name = "SnowflakeIdGenerator", strategy = "cc.iotkit.data.config.id.SnowflakeIdGenerator")
    private String id;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "地址")
    private String address;

    @ApiModelProperty(value = "联系人")
    private String linkMan;

    @ApiModelProperty(value = "联系电话")
    private String linkPhone;

    @ApiModelProperty(value = "描述")
    @Column(name = "`desc`")
    private String desc;

    @ApiModelProperty(value = "创建时间")
    private Long createAt;

    @ApiModelProperty(value = "创建人")
    private String createUserId;

    @ApiModelProperty(value = "修改时间")
    private Long modifyAt;

    @ApiModelProperty(value = "修改人")
    private String modifyUserId;

    @ApiModelProperty(value = "用户id")
    private String uid;


}
