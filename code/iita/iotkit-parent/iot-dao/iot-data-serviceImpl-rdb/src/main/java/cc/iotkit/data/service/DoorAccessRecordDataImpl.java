package cc.iotkit.data.service;

import cc.iotkit.common.api.PageRequest;
import cc.iotkit.common.api.Paging;
import cc.iotkit.data.dao.DoorAccessRecordRepository;
import cc.iotkit.data.dao.IJPACommData;
import cc.iotkit.data.model.TbDoorAccessRecord;
import cc.iotkit.data.record.IDoorAccessRecordData;
import cc.iotkit.data.util.PageBuilder;
import cc.iotkit.data.util.PredicateBuilder;
import cc.iotkit.model.report.DoorAccessRecord;
import com.querydsl.core.types.Predicate;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.List;

import static cc.iotkit.data.model.QTbAlertRecord.tbAlertRecord;
import static cc.iotkit.data.model.QTbDoorAccessRecord.tbDoorAccessRecord;


@Primary
@Service
@RequiredArgsConstructor
public class DoorAccessRecordDataImpl implements IDoorAccessRecordData, IJPACommData<DoorAccessRecord, Long> {


    private final DoorAccessRecordRepository doorAccessRecordRepository;

    @Override
    public JpaRepository getBaseRepository() {
        return doorAccessRecordRepository;
    }

    @Override
    public Class getJpaRepositoryClass() {
        return TbDoorAccessRecord.class;
    }

    @Override
    public Class getTClass() {
        return DoorAccessRecord.class;
    }


    @Override
    public List<DoorAccessRecord> selectDoorAccessRecordList(DoorAccessRecord convert) {
        Paging<DoorAccessRecord> recordPaging= PageBuilder.toPaging(doorAccessRecordRepository.findAll(buildQueryWrapper(convert), Pageable.unpaged()),DoorAccessRecord.class);
        return recordPaging.getRows();

    }

    @Override
    public Paging<DoorAccessRecord> findAll(PageRequest<DoorAccessRecord> pageRequest) {

        return PageBuilder.toPaging(
                doorAccessRecordRepository.findAll(
                        buildQueryWrapper(pageRequest.getData()),
                        PageBuilder.toPageable(pageRequest))
                , DoorAccessRecord.class
        );
    }

    private Predicate buildQueryWrapper(DoorAccessRecord record) {

        return PredicateBuilder.instance()
                .and(StringUtils.isNotBlank(record.getEmployeeName()), () -> tbDoorAccessRecord.employeeName.like("%" + record.getEmployeeName() + "%"))
                .and(StringUtils.isNotBlank(record.getDeviceName()), () -> tbDoorAccessRecord.deviceName.like("%" + record.getDeviceName() + "%"))
                .and(StringUtils.isNotBlank(record.getDeviceId()), () -> tbDoorAccessRecord.deviceId.like("%" + record.getDeviceId() + "%"))
                .and(StringUtils.isNotBlank(record.getUserType()), () -> tbDoorAccessRecord.deviceId.eq(record.getUserType()))
                .and(record.getDateTimeBegin() != null, () -> tbDoorAccessRecord.dateTime.goe(record.getDateTimeBegin()))
                .and(record.getDateTimeEnd() != null, () -> tbDoorAccessRecord.dateTime.loe(record.getDateTimeEnd()))
                .build();

    }
}
