package cc.iotkit.data.service;

import cc.iotkit.common.api.Paging;
import cc.iotkit.common.utils.MapstructUtils;
import cc.iotkit.data.dao.DeviceSupplierRepository;
import cc.iotkit.data.dao.IJPACommData;
import cc.iotkit.data.manager.IDeviceSupplierData;
import cc.iotkit.data.model.TbDeviceSupplier;
import cc.iotkit.model.device.DeviceSupplier;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Primary
@Service
@RequiredArgsConstructor
public class DeviceSupplierImpl implements IDeviceSupplierData, IJPACommData<DeviceSupplier, String> {


    private final DeviceSupplierRepository deviceSupplierRepository;


    @Override
    public JpaRepository getBaseRepository() {
        return deviceSupplierRepository;
    }

    @Override
    public Class getJpaRepositoryClass() {
        return TbDeviceSupplier.class;
    }

    @Override
    public Class getTClass() {
        return DeviceSupplier.class;
    }


    @Override
    public Paging<DeviceSupplier>  findByName(String name, int page, int size) {
        Page<TbDeviceSupplier> suppliers = deviceSupplierRepository.findByNameLike("%" + name.trim() + "%",
                Pageable.ofSize(size).withPage(page - 1));
        return new Paging<>(suppliers.getTotalElements(),
                MapstructUtils.convert(suppliers.getContent(), DeviceSupplier.class));
    }

    @Override
    public Paging<DeviceSupplier> findByConditions(String uid, int page, int size) {
        return null;
    }
}
