package cc.iotkit.data.service;

import cc.iotkit.common.api.PageRequest;
import cc.iotkit.common.api.Paging;
import cc.iotkit.common.utils.MapstructUtils;
import cc.iotkit.common.utils.ReflectUtil;
import cc.iotkit.data.dao.*;
import cc.iotkit.data.manager.ICategoryData;
import cc.iotkit.data.manager.IDeviceInfoData;
import cc.iotkit.data.manager.IManufacturersData;
import cc.iotkit.data.manager.IProductData;
import cc.iotkit.data.model.*;
import cc.iotkit.data.util.PageBuilder;
import cc.iotkit.model.device.DeviceGroup;
import cc.iotkit.model.device.DeviceInfo;
import cc.iotkit.model.device.Manufacturers;
import cc.iotkit.model.device.message.DevicePropertyCache;
import cc.iotkit.model.product.Category;
import cc.iotkit.model.product.Product;
import cc.iotkit.model.stats.DataItem;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

import static cc.iotkit.data.model.QTbDeviceGroupMapping.tbDeviceGroupMapping;
import static cc.iotkit.data.model.QTbDeviceInfo.tbDeviceInfo;
import static cc.iotkit.data.model.QTbDeviceSubUser.tbDeviceSubUser;
import static cc.iotkit.data.model.QTbProduct.tbProduct;

@Primary
@Service
@RequiredArgsConstructor
public class ManufacturersDataImpl implements IManufacturersData, IJPACommData<Manufacturers, String> {


    private final ManufacturersRepository manufacturersRepository;


    @Override
    public JpaRepository getBaseRepository() {
        return manufacturersRepository;
    }

    @Override
    public Class getJpaRepositoryClass() {
        return TbManufacturers.class;
    }

    @Override
    public Class getTClass() {
        return Manufacturers.class;
    }


    @Override
    public Paging<Manufacturers>  findByName(String name, int page, int size) {
        Page<TbManufacturers> groups = manufacturersRepository.findByNameLike("%" + name.trim() + "%",
                Pageable.ofSize(size).withPage(page - 1));
        return new Paging<>(groups.getTotalElements(),
                MapstructUtils.convert(groups.getContent(), Manufacturers.class));
    }

    @Override
    public Paging<Manufacturers> findByConditions(String uid, int page, int size) {
        return null;
    }
}
