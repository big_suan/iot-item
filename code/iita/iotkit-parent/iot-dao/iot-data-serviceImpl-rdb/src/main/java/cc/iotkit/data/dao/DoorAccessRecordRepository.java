package cc.iotkit.data.dao;

import cc.iotkit.data.model.TbDoorAccessRecord;
import cc.iotkit.model.report.DoorAccessRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface DoorAccessRecordRepository extends JpaRepository<TbDoorAccessRecord, Long>, QuerydslPredicateExecutor<DoorAccessRecord> {


}
