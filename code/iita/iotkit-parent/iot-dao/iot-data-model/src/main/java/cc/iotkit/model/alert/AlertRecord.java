/*
 * +----------------------------------------------------------------------
 * | Copyright (c) 奇特物联 2021-2022 All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed 未经许可不能去掉「奇特物联」相关版权
 * +----------------------------------------------------------------------
 * | Author: xw2sy@163.com
 * +----------------------------------------------------------------------
 */
package cc.iotkit.model.alert;

import cc.iotkit.model.Owned;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 告警配置
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AlertRecord implements Owned<Long> {

    private Long id;

    /**
     * 配置所属用户
     */
    private String uid;

    /**
     * 告警名称
     */
    private String name;

    /**
     * 告警严重度（1-5）
     */
    private String level;

    /**
     * 告警时间
     */
    private Long alertTime;

    /**
     * 告警时间--开始
     */
    private Long alertTimeStart;

    /**
     * 告警时间--结束
     */
    private Long alertTimeEnd;

    /**
     * 告警详情
     */
    private String details;

    /**
     * 是否已读
     */
    private Boolean readFlg;
    /**
     * 告警设备id
     */
    private String deviceId;
    /**
     * 告警设备名称
     */
    private String deviceName;
    /**
     * 告警类型：1报警、2、离线3、故障4、寿命到期,5、保养提醒
     */
    private String alarmType;

    /**
     * 告警类型：1报警、2、离线3、故障4、寿命到期5、保养提醒
     */
    private String alarmTypeName;
    /**
     * 楼层
     */
    private String floor;
}
