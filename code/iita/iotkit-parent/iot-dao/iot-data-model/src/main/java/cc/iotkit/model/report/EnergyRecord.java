package cc.iotkit.model.report;

import cc.iotkit.model.Id;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 温湿度
 */
@Data
public class EnergyRecord implements Id<Long>, Serializable {
    private Long id;

    private String deviceId;

    private String deviceName;


    private Date dateTime;

    private Date dateTimeBegin;

    private Date dateTimeEnd;



    /**
     * 所属平台用户ID
     */
    private String uid;


}
