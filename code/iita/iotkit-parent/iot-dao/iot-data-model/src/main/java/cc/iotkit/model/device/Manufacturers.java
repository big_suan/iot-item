package cc.iotkit.model.device;

import cc.iotkit.model.Owned;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Manufacturers implements Owned<String> {

    /**
     *
     */
    private String id;

    /**
     * 名称
     */
    private String name;
    /**
     * 地址
     */
    private String address;
    /**
     * 联系人
     */
    private String linkMan;
    /**
     * 联系电话
     */
    private String linkPhone;

    /**
     * 描述
     */
    private String desc;
    /**
     * 创建时间
     */
    private Long createAt;
    /**
     * 创建人
     */
    private String createUserId;
    /**
     * 修改时间
     */
    private Long modifyAt;
    /**
     * 修改人
     */
    private String modifyUserId;

    /**
     * 所属平台用户ID
     */
    private String uid;


}
