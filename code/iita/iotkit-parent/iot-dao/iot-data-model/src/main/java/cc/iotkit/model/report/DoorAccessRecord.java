package cc.iotkit.model.report;

import cc.iotkit.model.Id;
import cc.iotkit.model.Owned;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

/**
 * 门禁访问记录
 */
@Data
public class DoorAccessRecord implements Id<Long>, Serializable {

    private Long id;

    private String deviceId;

    private String deviceName;

    private String employeeNo;

    private String employeeName;

    private Date dateTime;

    private Date dateTimeBegin;

    private Date dateTimeEnd;

    private String userType;

    /**
     * 所属平台用户ID
     */
    private String uid;


}
