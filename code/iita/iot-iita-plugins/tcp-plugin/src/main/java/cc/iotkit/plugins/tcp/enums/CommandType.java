package cc.iotkit.plugins.tcp.enums;

import lombok.Getter;

@Getter
public enum CommandType {
    CODE_REGISTER("Start the process", 1),
    CODE_REGISTER_REPLY("Stop the process", 2),
    CODE_HEARTBEAT("Pause the process", 3),
    CODE_DATA_UP("Resume the process", 4),
    CODE_DATA_DOWN("Restart the process", 5);

    private final String description;
    private final int code;

    CommandType(String description, int code) {
        this.description = description;
        this.code = code;
    }



    @Override
    public String toString() {
        return this.name() + " (" + code + "): " + description;
    }
}
