package cc.iotkit.plugins.tcp.bean;

import cc.iotkit.plugins.tcp.parser.DeviceData;
import lombok.Data;

import java.util.List;

@Data
public class Payload {
    private List<? extends DeviceData>  deviceDataList;
}
