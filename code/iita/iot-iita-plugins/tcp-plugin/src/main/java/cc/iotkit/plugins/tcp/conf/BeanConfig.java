package cc.iotkit.plugins.tcp.conf;

import cc.iotkit.plugin.core.IPluginConfig;
import cc.iotkit.plugin.core.IPluginScript;
import cc.iotkit.plugin.core.LocalPluginConfig;
import cc.iotkit.plugin.core.LocalPluginScript;
import cc.iotkit.plugin.core.thing.IThingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @author sjg
 */
@Slf4j
@Component
public class BeanConfig {

    /**
     *  这个方法声明了一个 IThingService 类型的 bean。在开发模式下，它会返回一个 FakeThingService 的实例，这可能是一个模拟的、用于开发和测试目的的服务实现。
     * @return
     */
    @Bean
    @ConditionalOnProperty(name = "plugin.runMode", havingValue = "dev")//这是一个条件注解，它只有在满足特定条件时，才会创建相应的 bean。在这个例子中，只有当属性 plugin.runMode 的值为 dev（开发模式）时，以下方法才会创建 bean。
    IThingService getThingService() {
        return new FakeThingService();
    }

    /**
     * 这个方法声明了一个 IPluginScript 类型的 bean。它记录一条信息表示初始化了 LocalPluginScript，并返回这个对象的一个实例，这个实例使用了一个名为 script.js 的脚本文件
     * @return
     */
    @Bean
    @ConditionalOnProperty(name = "plugin.runMode", havingValue = "dev")
    IPluginScript getPluginScript() {
        log.info("init LocalPluginScript");
        return new LocalPluginScript("script.js");
    }

    /**
     * 一个用于开发模式下的本地插件配置。
     * @return
     */
    @Bean
    @ConditionalOnProperty(name = "plugin.runMode", havingValue = "dev")
    IPluginConfig getPluginConfig() {
        return new LocalPluginConfig();
    }



}
