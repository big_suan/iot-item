package cc.iotkit.plugins.tcp.enums;

import lombok.Getter;

@Getter
public enum DeviceType {
    TEMPERATURE_HUMIDITY_SENSORS("6kYp6jszrDns2yh4", 999),//todo 这里到时候需要根据新增到平台中的pk进行替换
    YOUREN_GATEWAY("EmMtH5y2m5SD4Ezp", 0),//网关
    AIR_CONDITIONER("nFAjy3fasCxKb2MG", 1),//空调
    FAN_FILTER_UNIT("DKEPDMRKfKYYN3sj", 2),//通风柜排风阀门
    FUME_HOOD("nK22NBSRfJeJ534d", 3),//排风机
    VENTILATION_DUCT("QctiSemh6r32tCmC", 4),//送风阀门
    ELECTRIC_METER("nR8kDNeBx5ir7dYX", 5),//电表
    YANG_DETECTOR("mGzG3JtJAPyFpG6B", 6),//氧气检测器
    KERAN_DETECTOR("MZzbQNBNkMKyJtkc", 7),//可燃气体检测器
    MAFL("cpjwbb82CAGF3SQp", 8),//马弗炉排风阀
    HUIFENG("XAFf7fj5SN6NBaJk", 9),//回风阀门
    XINFENG("3cBKiKJ6tGf5JD27", 11),//新风系统机组
    YALI("iSeJGWmfKsd4ad4G", 12);//压力

    private final String pk;
    private final int code;


    DeviceType(String pk, int code) {
        this.pk = pk;
        this.code = code;

    }


    public static String getPkByCode(int code) {
        for (DeviceType deviceType : DeviceType.values()) {
            if (deviceType.getCode() == code) {
                return deviceType.getPk();
            }
        }
        return null; // 或者抛出一个异常，如果适合你的应用逻辑
    }
}
