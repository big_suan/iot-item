package cc.iotkit.plugins.tcp.bean;

import lombok.Data;

@Data
public class ReportedData {
    private String name;
    private String value;
}
