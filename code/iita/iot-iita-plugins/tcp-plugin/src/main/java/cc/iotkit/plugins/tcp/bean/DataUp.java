package cc.iotkit.plugins.tcp.bean;

import cc.iotkit.plugins.tcp.parser.DeviceData;
import lombok.Data;

import java.util.List;

@Data
public class DataUp implements DeviceData {
   private String id;
   private String extensionName;
   private Integer code;
   private List<ReportedData> data;
}
