package cc.iotkit.plugins.tcp.parser;

import cc.iotkit.plugins.tcp.bean.Payload;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 数据包
 *
 * @author sjg
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EdgeComputingDataPackage {

    /**
     * 设备地址
     */
    private String addr;

    /**
     * 指令类型
     */
    private short code;


    /**
     * 包体数据
     */
    private Payload payload;



}
