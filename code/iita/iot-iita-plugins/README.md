# iot-iita-plugins



#### 使用说明
##### 首先配置有人网关 
###### 配置端口 
   1. 串口设置 
   波特率，数据位，停止位。说明书上会有根据说明书进行设置，串口模式基本都是rs485
![img.png](img.png)
![img_8.png](img_8.png)
    2.  配置socket
     1. 工作方式设置为tcpClient none
     2. 远程服务器地址设置为插件配置地址和端口
     3. 超时重连时间设置为0
     5. 网络打印关闭
     6. modbus轮询和使能网络心跳包不选择
     7. 注册包类型关闭
![img_7.png](img_7.png)
###### 注意！其他配置不动
###### 配置边缘计算网关
   1. 网关使能   
   设置为开启
 ![img_1.png](img_1.png)
   2. 数据采集   
设置分机，串口号，从机地址等，从机地址各设备有默认从机地址，如有重复需要修改，修改的话需要使用modbusPull软件
 ![img_2.png](img_2.png)
采集点填写 读取数据功能码03   
寄存器地址对应设备协议中的地址   
类型符号对应协议中的数据类型   
如果需要数据计算点击显示高级设置   
数据计算可以计算对应数据转换为实际值
![img_6.png](img_6.png)

   3. 数据上报   
通道选择刚刚配置好的socket   
数据查询开启，查询方式选择modbusRTU   
数据上报开启   
周期上报开启   
上报周期根据具体需求设置  
定时上报取消
失败填充取消
引号包含取消
![img_4.png](img_4.png)

value对应每个分机点位的代称
![img_5.png](img_5.png)

#### 注意！json格式
```json
    {
    "code": 4, //命令类型
    "gatewaySn": "SN123456789",  //网关序列号，通过发包工具获取 
    "extensionDetails": [ //所有设备-从机的集合
      {
    "id": 456,//分机点位  
    "code": 999, //设备类型 见备注
    "extensionName": "tempAndHumidity", //设备名称
    "reportedData": [ //当前设备的属性集合
      {  //属性上报
    "name": "temperature", //物模型属性
    "value": "node0101" //分机点位的代称
    },  {
    "name": "humidity",
    "value": "node0102"
    }]
    }, {
    "id": 4561,
    "extensionName": "aircondition",
    "code": 1,
    "reportedData": [{
    "name": "temperature",
    "value": "node0201"
    }, {
    "name": "coldOrHot",
    "value": "node0202"
    }, {
    "name": "oper",
    "value": "node0203"
    }]
    }]
    }
```
设备类型备注
有人网关 0
空调 1
风扇过滤器单元 2
风量阀门 3
通风_管道4
气体检测器 6
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
